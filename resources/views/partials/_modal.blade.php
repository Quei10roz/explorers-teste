<!-- Full Height Modal Top -->
<div class="modal fade top" id="fullHeightModalTop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full-height modal-top" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">Modal title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                
            <form action="#">
                <div class="commentbox blog-details-commentbox">   
                    <input type="text" name="commentbox-username" placeholder="Name">
                    <input type="email" name="commentbox-useremail" placeholder="Email"> 
                    <textarea name="commentbox-textarea" cols="30" rows="3" placeholder="Comments"></textarea>                           
                </div>
            </div>
                    <!--Footer-->
            <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Enviar</button>
            </form>
            </div>        
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Full Height Modal Top -->