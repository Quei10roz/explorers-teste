<!-- Header Area -->
<header id="header" class="header header-style-2 sticky-header header-transparent">
    <div class="container-fluid d-none d-lg-block">
        <div class="header-inner">
            <a href="index.html" class="logo">
                <img src="{{ asset('images/leandro/logo.png') }}" alt="logo">
            </a>            
            <nav class="mainmenu">
                <ul style="font-size:2px">
                    <li class="#">
                        <a href="/quem-somos">QUEM SOMOS</a>                        
                    </li>
                

                    <li class="cr-dropdown">
                        <a href="#">ORIGEM</a>
                        <ul>
                            <li>
                                <a href="/livro">LIVRO</a>                        
                            </li>

                            <li>
                                <a href="/jogo">JOGO</a>       
                            </li>  

                            <li>
                                <a href="/idealizador">IDEALIZADOR</a>       
                            </li>
                        </ul>
                    </li>

                        <li class="cr-dropdown">
                          <a href="/exploradores">EXPLORADORES</a>
                       </li>

                        <li class="cr-dropdown">
                          <a href="#">BÚSSOLAS</a>
                        <ul>
                            @foreach($nav_ofertas as $nav_oferta)
                                    <li>
                                        <a href="/bussulas/{{$nav_oferta->slug}}">{{$nav_oferta->nome}}</a>
                                    </li>
                            @endforeach                                
                        </ul>
                    </li>
                    <li class="cr-dropdown">
                        <a href="/diario">DIÁRIO DE BORDO</a>
                    </li>
                </ul>
            </nav>
            <div class="header-buttons">
                <a href="/login" class="cr-btn cr-btn-small cr-btn-white cr-btn-transparent">
                    <span>ÁREA DO EXPLORADOR</span>
                </a>
            </div>
        </div>
    </div>
    <div class="mobile-menu-wrap d-block d-lg-none">
        <div class="container">
            <div class="mobile-menu">
                <a href="index.html" class="mobile-logo">
                    <img src="{{ asset('images/leandro/logo.png') }}" alt="logo" style="width:150px">
                </a>
                <div class="header-buttons">
                    <ul>
                        <li>
                            <a href="#" class="cr-btn cr-btn-small">
                                <span>ÁREA DO EXPLORADOR</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!--// Header Area -->
<!--
<!-- Header Area 
<header id="header" class="header header-style-2 sticky-header header-transparent">
        <div class="container-fluid d-none d-lg-block">
            <div class="header-inner">
                <a href="/" class="logo">
                    <img src="{{ asset('images/leandro/logo.png') }}" alt="logo">
                </a>
                <nav class="mainmenu">
                    <ul>
                       <!--<li>
                            <a href="/">INÍCIO</a>
                        </li>-->
                       <!-- <li>
                            <a href="#">JOGO</a>
                        </li>
                        <li>
                            <a href="#">LIVRO</a>
                        </li>
                        <li>
                            <a href="/quem-somos">QUEM SOMOS</a>
                        </li>

                         <li class="cr-dropdown">
                                <a href="#">ORIGEM</a>
                                 <ul>

                                <li>
                                <a href="/livro">Livro</a>
                                </li>

                                <li>
                                <a href="/idealizador">Idealizador</a>
                                </li>

                            </ul>
                         </li>

                          <li>
                            <a href="/exploradores">EXPLORADORES</a>
                        </li>

                         <li class="cr-dropdown">
                           <a href="#">BÚSSOLAS</a>
                           <ul>
                               @foreach($nav_ofertas as $nav_oferta)
                                    <li>
                                        <a href="/bussulas/{{$nav_oferta->slug}}">{{$nav_oferta->nome}}</a>
                                    </li>
                               @endforeach                                
                            </ul>
                        </li>

                        <li class="cr-dropdown" >
                            <a href="/diario">DIÁRIO DE BORDO</a>
                           <!-- <ul>

                                <li>
                                <a href="idealizador">Idealizador</a>
                                </li>
                            </ul>
                        </li>
                    <li>
                <div class="header-buttons">
                    <a href="/login" class="cr-btn cr-btn-small cr-btn-white cr-btn-transparent">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;ÁREA DO EXPLORADOR &nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </a>
                </div>
            </li>
            </ul>
        </nav>

                <div class="header-buttons">
                    <a href="#" data-toggle="modal" data-target="#fullHeightModalTop" class="cr-btn cr-btn-small cr-btn-white cr-btn-transparent">
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;CONTATO &nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </a>
                </div>

            </div>
        </div>
        <div class="mobile-menu-wrap d-block d-lg-none">
            <div class="container">
                <div class="mobile-menu">
                    <a href="index.html" class="mobile-logo">
                        <img src="images/leandro/logo.png" alt="logo">
                    </a>
                    <div class="header-buttons">
                        <ul>
                            <li>
                                <a href="#" class="cr-btn cr-btn-small">
                                    <span>CONTATO</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--// Header Area -->

