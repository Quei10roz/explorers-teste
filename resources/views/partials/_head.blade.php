<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Explorers @yield('title')</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Jetpack Open Graph Tags -->
        <meta property="og:type" content="" />
        <meta property="og:title" content="Explorers" />
        <meta property="og:description" content="Rumo à nova economia do século 21" />
        <meta property="og:url" content="https:/" />
        <meta property="og:site_name" content="Explorers" />
        <meta property="og:image" content="https://marca-explorers.png" />
        <meta property="og:image:width" content="512" />
        <meta property="og:image:height" content="512" />
        <meta property="og:locale" content="pt_BR" />


        <!-- Favicons -->
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="apple-touch-icon" href="images/icon.png">
    
        <!-- Google font (font-family: 'Open Sans', sans-serif;) -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300i,400,400i,700,800" rel="stylesheet">
        <!-- Google font (font-family: 'Poppins', sans-serif;) -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,700" rel="stylesheet">
    
        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/plugins.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    
        <!-- Cusom css -->
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    
        <!-- Modernizer js -->
        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        
    </head>