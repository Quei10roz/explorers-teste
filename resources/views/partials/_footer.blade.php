<!-- Footer Area -->
<footer class="footer-area">
<!-- Footer Copyright Area -->
    <div class="footer-copyright-area bg-theme">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright text-center">
                        <p>Copyright &copy; 2018
                            <a href="#">explorers.digital</a>
                        </p>
                    </div>
                    <div class="social-icons social-icons-rounded social-icons-transparent text-center">
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>                          
                        </ul>
                    </div>
                    <div class="text-center">
                        <a href="#" class="cr-btn cr-btn-small cr-btn-white cr-btn-transparent">
                            <span>CONTATO</span>
                        </a>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Footer Copyright Area -->
    @extends('partials._modal')
</footer>

<!--// Footer Area -->