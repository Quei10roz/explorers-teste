<table class="table table-responsive" id="tags-table">
    <thead>
        <tr>
            <th>Nome</th>
        </tr>
    </thead>
    <tbody>
    @foreach($tags as $tags)
        <tr>
            <td>{!! $tags->nome !!}</td>
            <td>
                {!! Form::open(['route' => ['tags.destroy', $tags->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('tags.edit', [$tags->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza que quer fazer isso?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>