<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Titulo:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<!-- Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tags', 'Tags (opcional):') !!}
    {!! Form::text('tags', null, ['class' => 'form-control']) !!}
</div>

<!-- Conteudo Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('conteudo', 'Conteúdo:') !!}
    {!! Form::textarea('conteudo', null, ['class' => 'form-control']) !!}
</div>

<!-- Imagem Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Imagem (opcional):') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>

<!-- Link-Video Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link_video', 'Link para Video (opcional):') !!}
    {!! Form::text('link_video', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('posts.index') !!}" class="btn btn-default">Cancelar</a>
</div>
