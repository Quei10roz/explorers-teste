<table class="table table-responsive" id="posts-table">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Autor</th>
            <th>Criado em</th>
        </tr>
    </thead>
    <tbody>
    @foreach($posts as $post)
        <tr>
            <td>{{ substr($post->titulo, 0, 30) }}{{ strlen($post->titulo) > 30 ? "..." : "" }}</td>        
            <td>{{ substr(App\User::find($post->user_id)->name, 0, 15) }}{{ strlen(App\User::find($post->user_id)->name) > 15 ? "..." : "" }}</td>
            <td>{!! date('j M Y', strtotime($post->created_at)) !!}</td>
            <td>
                {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('comentariospost', [$post->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-comment"></i></a>
                    <a href="{!! route('posts.show', [$post->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('posts.edit', [$post->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza que quer fazer isso?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">
    {!! $posts->links() !!}
</div>