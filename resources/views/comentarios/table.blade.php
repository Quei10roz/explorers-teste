<table class="table table-responsive" id="comentarios-table">
    <thead>
        <tr>
            <th>Autor</th>
            <th>Conteudo</th>
            <th>Post</th>
            <th>Autor do Post</th>
        </tr>
    </thead>
    <tbody>
    @foreach($comentarios as $comentario)
        <tr>
            <td>{{ substr($comentario->autor, 0, 20) }}{{ strlen($comentario->autor) > 20 ? "..." : "" }}</td>
            <td>{{ substr($comentario->conteudo, 0, 20) }}{{ strlen($comentario->conteudo) > 20 ? "..." : "" }}</td>
            <td>{{ substr(App\Post::find($comentario->post_id)->titulo, 0, 30) }}{{ strlen(App\Post::find($comentario->post_id)->titulo) > 30 ? "..." : "" }}</td>
            <td>{{ substr(App\Post::find($comentario->post_id)->user->name, 0, 15) }}{{ strlen(App\Post::find($comentario->post_id)->user->name) > 15 ? "..." : "" }}</td>
            <td>
                {!! Form::open(['route' => ['comentarios.destroy', $comentario->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('comentarios.show', [$comentario->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza que quer fazer isso?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="text-center">
    {!! $comentarios->links() !!}
</div>