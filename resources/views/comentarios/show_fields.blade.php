<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $comentarios->id !!}</p>
</div>

<!-- Autor Field -->
<div class="form-group">
    {!! Form::label('autor', 'Autor:') !!}
    <p>{!! $comentarios->autor !!}</p>
</div>

<!-- Conteudo Field -->
<div class="form-group">
    {!! Form::label('conteudo', 'Conteudo:') !!}
    <p>{!! $comentarios->conteudo !!}</p>
</div>

<!-- Post Id Field -->
<div class="form-group">
    {!! Form::label('post_id', 'Post Id:') !!}
    <p>{!! $comentarios->post_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $comentarios->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $comentarios->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $comentarios->deleted_at !!}</p>
</div>

