@extends('main')
<!-- CSS Style -->
@section('content') 

    <!-- Breadcrumb Area -->
    <div class="breadcrumb-area bg-breadcrumb-8 section-padding-xlg" data-black-overlay="5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cr-breadcrumb text-center">
                            <!--<h6>heart of business loving people</h6>-->
                            <h1>DIÁRIO DE BORDO</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Breadcrumb Area -->

        <!-- Start Page Content -->
        <main class="page-content">

            <!-- Section -->
            <div class="pg-blogs-area section-padding-lg bg-white">
                <div class="container">
                    <div class="row">
                        <!-- Blog List Blogs -->
                        <div class="col-lg-8 col-12 sticky-sidebar-content">
                            <div class="list-blogs-wrap">
                                <div class="row">
                                    
                                    <div class="col-12">
                                        <!-- Single Blog -->
                                        <article class="grid-blog sticky list-blog text-center">
                                            <div class="grid-blog-header">
                                                <div class="grid-blog-share">
                                                    <span>SHARE BY : </span>
                                                    <div class="social-icons social-icons-rounded">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-blog-thumb-wrap">
                                                <div class="grid-blog-thumb">
                                                    <img src="images/blog/list-blog/grid-blog-1-thumb.jpg" alt="blog thumb">
                                                </div>
                                            </div>
                                            <div class="grid-blog-content">
                                                <div class="grid-blog-meta">
                                                    <span>
                                                        <a href="blog-grid.html">Corporate</a>
                                                    </span>
                                                    <span>Post By :
                                                        <a href="blog-grid.html">Stela Hokings</a>
                                                    </span>
                                                </div>
                                                <h5 class="grid-blog-title">
                                                    <a href="blog-details.html">How to improve quality and customer service?</a>
                                                </h5>
                                                <p> quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquaqurat voluptatem.</p>
                                                <div class="grid-blog-likes-and-comments">
                                                    <a href="#">Like : 10</a>
                                                    <a href="#">Comments : 10</a>
                                                </div>
                                                <a href="#" class="cr-btn cr-btn-small cr-btn-dark">
                                                    <span>Read More</span>
                                                </a>
                                            </div>
                                        </article>
                                        <!--// Single Blog -->
                                    </div>

                                    <div class="col-12">
                                        <!-- Single Blog -->
                                        <article class="grid-blog list-blog text-center">
                                            <div class="grid-blog-header">
                                                <div class="grid-blog-share">
                                                    <span>SHARE BY:</span>
                                                    <div class="social-icons social-icons-rounded">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-blog-thumb-wrap">
                                                <div class="grid-blog-thumb">
                                                    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/72530897&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=true&amp;visual=true"></iframe>
                                                </div>
                                            </div>
                                            <div class="grid-blog-content">
                                                <div class="grid-blog-meta">
                                                    <span>
                                                        <a href="blog-grid.html">Corporate</a>
                                                    </span>
                                                    <span>Post By :
                                                        <a href="blog-grid.html">Stela Hokings</a>
                                                    </span>
                                                </div>
                                                <h5 class="grid-blog-title">
                                                    <a href="blog-details.html">First things for business how to improve your experience</a>
                                                </h5>
                                                <p> quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquaqurat voluptatem.</p>
                                                <div class="grid-blog-likes-and-comments">
                                                    <a href="#">Like : 10</a>
                                                    <a href="#">Comments : 10</a>
                                                </div>
                                                <a href="#" class="cr-btn cr-btn-small cr-btn-dark">
                                                    <span>Read More</span>
                                                </a>
                                            </div>
                                        </article>
                                        <!--// Single Blog -->
                                    </div>

                                    <div class="col-12">
                                        <!-- Single Blog -->
                                        <article class="grid-blog grid-blog-video list-blog text-center">
                                            <div class="grid-blog-header">
                                                <div class="grid-blog-share">
                                                    <span>SHARE BY:</span>
                                                    <div class="social-icons social-icons-rounded">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-blog-thumb-wrap">
                                                <div class="grid-blog-thumb">
                                                    <img src="images/blog/list-blog/grid-blog-2-thumb.jpg" alt="blog thumb">
                                                    <a href="https://vimeo.com/143996226" class="cr-video-btn cr-video-btn-dark cr-video-btn-small video-popup-trigger">
                                                        <i class="fa fa-play"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="grid-blog-content">
                                                <div class="grid-blog-meta">
                                                    <span>
                                                        <a href="blog-grid.html">Corporate</a>
                                                    </span>
                                                    <span>Post By :
                                                        <a href="blog-grid.html">Stela Hokings</a>
                                                    </span>
                                                </div>
                                                <h5 class="grid-blog-title">
                                                    <a href="blog-details.html">See our videos to know more about anything.</a>
                                                </h5>
                                                <p> quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquaqurat voluptatem.</p>
                                                <div class="grid-blog-likes-and-comments">
                                                    <a href="#">Like : 10</a>
                                                    <a href="#">Comments : 10</a>
                                                </div>
                                                <a href="#" class="cr-btn cr-btn-small cr-btn-dark">
                                                    <span>Read More</span>
                                                </a>
                                            </div>
                                        </article>
                                        <!--// Single Blog -->
                                    </div>

                                    <div class="col-12">
                                        <!-- Single Blog -->
                                        <article class="grid-blog list-blog text-center">
                                            <div class="grid-blog-header">
                                                <div class="grid-blog-share">
                                                    <span>SHARE BY:</span>
                                                    <div class="social-icons social-icons-rounded">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-blog-thumb-wrap">
                                                <div class="grid-blog-thumb">
                                                    <img src="images/blog/list-blog/grid-blog-3-thumb.jpg" alt="blog thumb">
                                                </div>
                                            </div>
                                            <div class="grid-blog-content">
                                                <div class="grid-blog-meta">
                                                    <span>
                                                        <a href="blog-grid.html">Corporate</a>
                                                    </span>
                                                    <span>Post By :
                                                        <a href="blog-grid.html">Stela Hokings</a>
                                                    </span>
                                                </div>
                                                <h5 class="grid-blog-title">
                                                    <a href="blog-details.html">How to design corporate profile to run a startup business?</a>
                                                </h5>
                                                <p> quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquaqurat voluptatem.</p>
                                                <div class="grid-blog-likes-and-comments">
                                                    <a href="#">Like : 10</a>
                                                    <a href="#">Comments : 10</a>
                                                </div>
                                                <a href="#" class="cr-btn cr-btn-small cr-btn-dark">
                                                    <span>Read More</span>
                                                </a>
                                            </div>
                                        </article>
                                        <!--// Single Blog -->
                                    </div>

                                </div>

                                <div class="row justify-content-center">
                                    <div class="col-xl-8 col-lg-10 col-12">
                                        <div class="cr-pagination mt-5 text-center">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">1</a>
                                                </li>
                                                <li>
                                                    <a href="#">2</a>
                                                </li>
                                                <li>
                                                    <a href="#">3</a>
                                                </li>
                                                <li>
                                                    <a href="#">...</a>
                                                </li>
                                                <li>
                                                    <a href="#">28</a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--// Blog List Blogs -->

                        <!-- Blog List Sidebar -->
                        <div class="col-lg-4 col-12 sticky-sidebar-sidebar">
                            <div class="widgets right-sidebar">
                                
                                <!-- Single Widget -->
                                <div class="single-widget widget-search">
                                    <form action="#">
                                        <input type="text" placeholder="Search">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                                <!--// Single Widget -->

                                <!-- Single Widget -->
                                <div class="single-widget widget-about">
                                    <h5 class="widget-title">About</h5>
                                    <img src="images/others/about-widget-image.jpg" alt="about widget thumb">
                                    <p>Lorem ipsum dolor sit amet, cadipisicing elit, sed do eiusmod tempor incididunt labore et dolore magna aliqua.labore et dolore.</p>
                                    <a href="#" class="cr-readmore">Read More.</a>
                                </div>
                                <!--// Single Widget -->

                                <!-- Single Widget -->
                                <div class="single-widget widget-recent-post">
                                    <h5 class="widget-title">Recent Post</h5>
                                    <ul>
                                        <li>
                                            <div class="recent-post-thumb">
                                                <img src="images/blog/recent-post/recent-post-thumb-1.png" alt="recent post thumb">
                                            </div>
                                            <div class="recent-post-content">
                                                <h6><a href="blog-details.html">Onsectetur, Adpisc in Velit, Sed.</a></h6>
                                                <span><a href="blog-list.html">August 01, 2017</a></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="recent-post-thumb">
                                                <img src="images/blog/recent-post/recent-post-thumb-2.png" alt="recent post thumb">
                                            </div>
                                            <div class="recent-post-content">
                                                <h6><a href="blog-details.html">Onsectetur, Adpisc in Velit, Sed.</a></h6>
                                                <span><a href="blog-list.html">August 01, 2017</a></span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="recent-post-thumb">
                                                <img src="images/blog/recent-post/recent-post-thumb-3.png" alt="recent post thumb">
                                            </div>
                                            <div class="recent-post-content">
                                                <h6><a href="blog-details.html">Onsectetur, Adpisc in Velit, Sed.</a></h6>
                                                <span><a href="blog-list.html">August 01, 2017</a></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!--// Single Widget -->

                                <!-- Single Widget -->
                                <div class="single-widget widget-categories">
                                    <h5 class="widget-title">Categories</h5>
                                    <select>
                                        <option data-display="Corporate">Corporate</option>
                                        <option value="1">Business</option>
                                        <option value="2">Finance</option>
                                        <option value="3">Agency</option>
                                        <option value="4">Model</option>
                                    </select>
                                </div>
                                <!--// Single Widget -->

                                <!-- Single Widget -->
                                <div class="single-widget widget-tags">
                                    <h5 class="widget-title">Tags</h5>
                                    <ul>
                                        <li><a href="blog-details.html">Fashion</a></li>
                                        <li><a href="blog-details.html">Business</a></li>
                                        <li><a href="blog-details.html">Agency</a></li>
                                        <li><a href="blog-details.html">Model Agency</a></li>
                                        <li><a href="blog-details.html">Life Style</a></li>
                                        <li><a href="blog-details.html">Blog</a></li>
                                        <li><a href="blog-details.html">Planning</a></li>
                                        <li><a href="blog-details.html">Graphics</a></li>
                                    </ul>
                                </div>
                                <!--// Single Widget -->

                                <!-- Single Widget -->
                                <div class="single-widget widget-instagram">
                                    <h5 class="widget-title">Follow Us On Instagram</h5>
                                    <ul id="sidebar-instagram-feed"></ul>
                                </div>
                                <!--// Single Widget -->

                            </div>
                        </div>
                        <!--// Blog List Sidebar -->
                    </div>

                </div>
            </div>
            <!--// Section -->

        </main>
        <!--// Start Page Content -->
        @endsection