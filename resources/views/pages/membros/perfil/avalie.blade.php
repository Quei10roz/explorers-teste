@extends('main')

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
@section('content')

<!-- Banner Area -->
<div class="banner-area ">
    <!-- Single Banner -->
    <div class="" data-black-overlay="7" data-speed="0.7" style="background-image: url(images/leandro/qs.jpg);height: 120px">
    </div>
    <!--// Single Banner -->
</div>
<!--// Banner Area -->


<main class="page-content">
    <!-- Contact Area -->
			<section class="cr-section contact-area bg-grey">
				<div class="row no-gutters align-items-center">
					<div class="col-lg-6 col-12 order-2 order-lg-1">
						<div class="promocontent mt-5 mt-lg-0">
							<div class="section-title section-title-light-theme">
								<h2>Nome do Evento</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed doeiusmod tdunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nexercitation ulboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor i rehenderit.</p>
							</div>

							<form class="contact-form" action="#">
                                <div class="estrelas">
                                    <h2>
                                        <input type="radio" id="cm_star-empty" name="fb" value="" checked/>
                                        <label for="cm_star-1"><i class="fa"></i></label>
                                        <input type="radio" id="cm_star-1" name="fb" value="1"/>
                                        <label for="cm_star-2"><i class="fa"></i></label>
                                        <input type="radio" id="cm_star-2" name="fb" value="2"/>
                                        <label for="cm_star-3"><i class="fa"></i></label>
                                        <input type="radio" id="cm_star-3" name="fb" value="3"/>
                                        <label for="cm_star-4"><i class="fa"></i></label>
                                        <input type="radio" id="cm_star-4" name="fb" value="4"/>
                                        <label for="cm_star-5"><i class="fa"></i></label>
                                        <input type="radio" id="cm_star-5" name="fb" value="5"/>
                                    </h2>
                                </div>
                                    <textarea name="" id="" cols="30" rows="10"></textarea>
                                    <div class="col-md-3 nav navbar-right">
                                    <input name="Enviar" id="" class="btn btn-primary btn-sm " type="button" value="Enviar">
                                    </div>
							</form>
						</div>
					</div>

					<div class="col-lg-6 col-12 order-1 order-lg-2">
						<div class="promoimage promoimage-animated contact-details">
							<img src="images/leandro/membros/david.png" alt="contact promothumb">
						</div>
                       
						</div>
					</div>
				</div>
			</section>
			<!--// Contact Area -->

</main>

@endsection