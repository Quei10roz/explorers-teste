@extends('main')

@section('content')

<!-- Breadcrumb Area -->
<div class="breadcrumb-area section-padding-xlg jarallax" data-black-overlay="7" style="background-image:url('/images/leandro/oferta.png')">
    <div class="container">
        <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                        <div class="team">
                    <!-- Single Team -->                            
                        <div class="team-thumb text-center">
                            @if ($user->image != null)
                                <img src="{{'/fotos_users/'.$user->image}}" class="text-center" alt="User Image"/>
                            @else
                            <!-- Foto Padrao -->
                                <img src="/fotos_users/padrao.jpg" alt="logo explorers"/>
                            @endif
                        </div>                            
                    </div>
                    <!--// Single Team -->
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                    <h1 style='color:aliceblue'>{{ $user->name }}</h1>
                    <h6 style='color:aliceblue'>{{ $user->cidade }}</h6>
                </div>
                <div class="social-icons social-icons-rounded social-icons-transparent text-center">
                        <ul>
                            @if($user->facebook != null)
                                <li>
                                    <a href="{{$user->facebook}}">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            @endif
                            @if($user->twitter != null)
                                <li>
                                    <a href="{{$user->twitter}}">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                            @endif
                            @if($user->linkedin != null)
                                <li>
                                    <a href="{{$user->linkedin}}">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                            @endif
                            @if($user->instagram != null)
                                <li>
                                    <a href="{{$user->instagram}}">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
            </div>
        </div>
    </div>

<!--// Breadcrumb Area -->


                    <div class="testimonial-wrap testimonial-style-3 testimonial-slider-active ">

                        <!-- Single Testimonial -->
                        <div class="testimonial text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing sed usmod tempor incididunt ut labam
                                est, qui dolor sit amet, conse tetur,lore magnaluptatem.</p>
                            
                        </div>
                        <!--// Single Testimonial -->

                        <!-- Single Testimonial -->
                        <div class="testimonial text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing sed usmod tempor incididunt ut labam
                                est, qui dolor sit amet,lore magnaluptatem consectetur.</p>
                           
                        </div>
                        <!--// Single Testimonial -->

                        <!-- Single Testimonial -->
                        <div class="testimonial text-center">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing sed usmod tempor incididunt ut labam
                                est, qui dolor sit amet, conse tetur amet.</p>
                          
                        </div>
                        <!--// Single Testimonial -->

                    </div>
                 </div>
             </div>
    </section>
    <!--// Testimonial Area -->

    <!-- Work experience area -->
    <section class="cr-section work-experience-area section-padding-xlg jarallax" data-black-overlay="7" style="background-image:url('/images/leandro/4.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-12">
                    <div class="section-title-2 text-center section-title-white">
                        <h3>SOBRE MIM</h3>
                        <p style="overflow:clip">{{ $user->descricao }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--// Work experience area -->
    <!-- Brand Logos -->
    <div class="cr-section brand-logos-area bg-grey section-padding-xs">
        <div class="container">
            <div class="col-lg-10 offset-lg-1 col-12">
                <div class="section-title section-title-light-theme text-center">
                    <h2>Minhas Ofertas</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="brand-logos slider-navigation-style-1">
                        @foreach($ofertas as $oferta)
                            <div class="single-brand-logo">
                                <a href="#">
                                    <img src="/images/leandro/logol.png" alt="teste">
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Brand Logos -->

     <!-- Work experience area -->
     <div class="cr-section work-experience-area bg-image-17 section-padding-xlg jarallax" data-black-overlay="7">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 col-12">
                            <div class="section-title-2 text-center section-title-white">
                                <h3>MEUS EVENTOS</h3>
                               
                            </div>
                        </div>
                    </div>

                 <!-- Portfolio Related Projects -->
          
            
                    <div class="row justify-content-center">
                    @foreach($eventos as $evento)
                        <div class="col-lg-4 col-md-6 col-12">
                                <!-- Signle Project -->
                            <div class="related-portfolio text-center">
                                <a href="{{ route('pagina.evento',[$evento->slug]) }}" class="related-portfolio-thumb">
                                    <img src="{{ '/fotos_ofertas/'.\App\Oferta::find($evento->oferta_id)->image_banner }}" alt="Related Project">
                                </a>

                                <div class="related-portfolio-content">
                                    <h5><a href="{{ route('pagina.evento',[$evento->slug]) }}">{{ $evento->titulo }}</a></h5>
                                    <h6>{{ $evento->cidade }}</h6>
                                </div>
                            </div>    
                            <!--// Signle Project -->  
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <!--// Portfolio Related Projects -->








                    
               
            <!--// Porjects Area -->    

           <!-- <div class="portfolios4 row mt-4 portfolio-popup-gallery">

            <!-- Single Portfolio 
            @foreach($eventos as $evento)
            <div class="portfolio-single col-xl-3 col-md-6 col-12">
                <div class="portfolio portfolio-style-4">
                     <div class="portfolio-thumb">
                            <img src="images/portfolio/portfolio-gallery-5/portfolio-gallery5-thumb-1.jpg" alt="portfolio thumb">
                        </div>

                        <div class="portfolio-content text-center">
                            <div class="portfolio-content-inner">
                                <h4>
                                    <a href="images/portfolio/portfolio-gallery-5/portfolio-gallery5-thumb-1.jpg"> {{ $evento->titulo }}</a>
                                </h4>
                                <h6>{{ $evento->cidade }}</h6>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
<!--// Single Portfolio -->

            </section>
            <!--// Work experience area -->

    <!-- Brand Logos 
    <div class="cr-section brand-logos-area bg-grey section-padding-xs jarallax" data-black-overlay="7" style="background-image:url('/images/leandro/10.jpg')">
        <div class="container">
            <div class="col-lg-10 offset-lg-1 col-12">
                <div class="section-title section-title-light-theme text-center">
                    <h2 style="color:aliceblue">Meus Eventos</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12 ">
                    <div class="brand-logos slider-navigation-style-1">
                        <div class="center">
                            @foreach($eventos as $evento)
                                <!-- ITEM 
                                <div class="single-brand-logo">
                                    <a href="#">
                                        <div class="service service-style-2">
                                            <div class="service-content">
                                                <p>
                                                    {{ $evento->titulo }}
                                                </p>
                                                <h6>{{ $evento->cidade }}</h6>
                                                <p>{!! date('j M Y', strtotime($evento->data)) !!}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div> 
                                <!-- ITEM  
                            @endforeach
                        </div> -->                         
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Brand Logos -->
</main>
<!--// Start Page Content -->
@endsection