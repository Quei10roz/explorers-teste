@extends('main')

@section('title','Ofertas')

@section('content') 

<!-- Breadcrumb Area -->
<div class="breadcrumb-area section-padding-xlg" data-black-overlay="5" style="background-image: url({{ '/fotos_ofertas/'.\App\Oferta::find($evento->oferta_id)->image_banner }}); background-repeat:no-repeat; background-size: cover;">
        
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cr-breadcrumb text-center">
                    <!--<h6>heart of business loving people</h6>-->
                    <h1>{{ $evento->titulo }}</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Breadcrumb Area -->

<!-- Models Area -->
<section class="cr-section service-area bg-grey">
        <div class="row no-gutters">
            <div class="col-xl-6 col-12 order-1 order-xl-2">
                <div class="promoimage models-thumb-slider-active slider-navigation-style-3">
                    <img src="images/models/model-1.jpg" alt="promo image">
                </div>
            </div>
            <div class="col-xl-6 col-12 order-2 order-xl-1 models-wrap" data-boxtitle="Model" data-white-overlay="8">
                <div class="promocontent promocontent-5">

                    <div class="section-title section-title-light-theme ">
                        <h2>Informações do Evento</h2>
                    </div>

                    <div class="models models-details-slider-active">

                        <!-- Single Model Deion -->
                        <div class="model-des-single">
                            <div class="model-des">
                                <div class="model-des-thumb">                                   
                                    @if (\App\User::find($evento->user_id)->image != null)
                                        <img src="{{('fotos_users/'.\App\User::find($evento->user_id)->image) }}" alt="model thumb">     
                                    @endif
                                </div>

                                <div class="model-des-content">
                                    <h6>Explorador: {{ \App\User::find($evento->user_id)->name }}</h6>
                                    <ul>
                                        <li>Cidade :
                                        <span>{{ $evento->cidade }}</span>
                                        </li>
                                        <li>Data :
                                            <span>{!! date('j M Y', strtotime($evento->data)) !!}</span>
                                        </li>
                                    </ul>
                                    <p>{{ $evento->descricao }}</p>                                    
                                </div>
                            </div>
                            <div class="col-ms-6 text-center ">
                                <a href="{{ $evento->link }}" class="btn btn-primary ">Ver Mais</a>
                            </div>
                        </div>
                        <!--// Single Model Deion -->

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--// Models Area -->


@endsection