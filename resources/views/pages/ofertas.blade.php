@extends('main')

@section('title','Ofertas')

@section('content') 

<!-- Main wrapper -->
<div class="wrapper" id="wrapper">

    <!-- Start Page Content -->
    <main class="page-content">

        <!-- Banner Area -->
		<div class="banner-area">

            <div class="banner-slider-active-with-navigation slider-navigation-style-3">

                <!-- Single Banner -->
                <div class="banner bg-image-8 fullscreen jarallax" data-black-overlay="5" data-speed="0.7" style="background-image: url({{ asset('/fotos_ofertas/'.$oferta->image_banner) }});">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-2 col-md-12 offset-lg-1 col-12 offset-0">
                                <div class="banner-content text-center cr-tilter">
                                    <!--<h1>{{ $oferta->nome }}</h1>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--// Single Banner -->
            </div>
        </div>
<!--// Banner Area -->




        <!-- Portfolio Details Area
        <div class="pg-portfolio-area section-padding-lg bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="pg-portfolio-thumbs slider-navigation-style-4">
                            <div class="pg-portfolio-thumb-single">
                                <img src="images/leandro/jogo.jpeg" alt="portfolio thumb">
                            </div>
                            <div class="pg-portfolio-thumb-single">
                                <img src="images/leandro/jogo.jpeg" alt="portfolio thumb">
                            </div>
                            <div class="pg-portfolio-thumb-single">
                                <img src="images/leandro/jogo.jpeg" alt="portfolio thumb">
                            </div>
                        </div>-->

                        <div class="pg-portfolio-content text-center">
                            <div class="section-title-4 text-center">
                                <h2>Descrição</h2>
                            </div>
                            <p>{{ $oferta->descricao }}</p>
                            <!--<blockquote>
                                <p>it, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation modo consequ.</p>
                            </blockquote>-->
                        </div>
                        <br>
                        
                        <div class="pg-portfolio-details text-center">
                            <div class="pg-portfolio-details-box">
                            <div class="row mt-1 justify-content-center">


                                <!-- Blog Area -->
                                <section class="cr-section blog-area bg-white section-padding-xlg">
				<div class="container">
					<div class="row">
						<div class="col-lg-10 offset-lg-1 col-12">
							<div class="section-title section-title-light-theme text-center">
								<h2>Eventos</h2>
							</div>
						</div>
					</div>

					<div class="row blog-slider-active slider-navigation-style-1 team-gallery">

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2 team">
								<div class=" team-thumb ">
									<img src="{{ asset('images/leandro/membros/david.png') }}" alt="blog thumb">
                                </div>
                                
								<div class="blog-content">
									<h5 class="blog-title">
										<a href="usuário">David Meth</a>
                                    </h5>
                                    <small class="text-white">Salvador</small>
								</div>
							</div>
							<!--// Single Blog -->
						</div>

			

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2  team">
								<div class="team-thumb">
									<img src="images/leandro/membros/jorge.jpg" alt="blog thumb">
								</div>
								<div class="blog-content">
                                <div class="blog-content">
									<h5 class="blog-title">
										<a href="blog-details.html">Jorge Nolasco</a>
                                    </h5>
                                    <small class="text-white">Salvador</small>
								</div>
					
								</div>
							</div>
							<!--// Single Blog -->
						</div>

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2 team">
								<div class="team-thumb">
									<img src="images/leandro/membros/karina.png" alt="blog thumb">
								</div>
								<div class="blog-content">
                                    <h5 class="blog-title">
										<a href="blog-details.html">Karina Papa</a>
                                    </h5>
                                    <small class="text-white">São Paulo</small>
								</div>
							</div>
							<!--// Single Blog -->
						</div>

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2 team">

								<div class="team-thumb">
									<img src="images/leandro/membros/Calura.jpg" alt="blog thumb">
								</div>
								<div class="blog-content">
									
									<h5 class="blog-title">
										<a href="blog-details.html">Sergio Calura</a>
                                    </h5>
                                    <small class="text-white">Goiânia</small>
								
								</div>
							</div>
							<!--// Single Blog -->

						</div>

					</div>
				</div>
			</section>
			<!--// Blog Area -->


              <!-- Blog Area -->
              <section class="cr-section blog-area bg-white section-padding-xlg">
				<div class="container">
					<div class="row">
						<div class="col-lg-10 offset-lg-1 col-12">
							<div class="section-title section-title-light-theme text-center">
								<h2>Membros ofertantes</h2>
							</div>
						</div>
					</div>

					<div class="row blog-slider-active slider-navigation-style-1 team-gallery">

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2 team">
								<div class=" team-thumb ">
								
                                </div>
                                
								<div class="blog-content">
									<h5 class="blog-title">
										<a href="usuário">David Meth</a>
                                    </h5>
                                    <small class="text-white">Salvador</small>
								</div>
							</div>
							<!--// Single Blog -->
						</div>

			

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2  team">
								<div class="team-thumb">
									<img src="images/leandro/membros/jorge.jpg" alt="blog thumb">
								</div>
								<div class="blog-content">
                                <div class="blog-content">
									<h5 class="blog-title">
										<a href="blog-details.html">Jorge Nolasco</a>
                                    </h5>
                                    <small class="text-white">Salvador</small>
								</div>
					
								</div>
							</div>
							<!--// Single Blog -->
						</div>

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2 team">
								<div class="team-thumb">
									<img src="images/leandro/membros/karina.png" alt="blog thumb">
								</div>
								<div class="blog-content">
                                    <h5 class="blog-title">
										<a href="blog-details.html">Karina Papa</a>
                                    </h5>
                                    <small class="text-white">São Paulo</small>
								</div>
							</div>
							<!--// Single Blog -->
						</div>

						<div class="col-lg-4 col-md-6 col-12">
							<!-- Single Blog -->
							<div class="blog-item-2 team">

								<div class="team-thumb">
									<img src="images/leandro/membros/Calura.jpg" alt="blog thumb">
								</div>
								<div class="blog-content">
									
									<h5 class="blog-title">
										<a href="blog-details.html">Sergio Calura</a>
                                    </h5>
                                    <small class="text-white">Goiânia</small>
								
								</div>
							</div>
							<!--// Single Blog -->

						</div>

					</div>
				</div>
			</section>
			<!--// Blog Area -->

            
			<!--// Blog Area -->
                            
                                <!--<ul>
                                    <li>Membro ofertante:
                                        <span>Leandro Jesus</span>
                                    </li>
                                    <li>Cidade :
                                        <span>São Pauli</span>
                                    </li>
                                    <li>Category :
                                        <span>Stiphen Howkings</span>
                                    </li>
                                    <li>Category :
                                        <span>
                                            <a href="#">http://www.stipheen.com</a>
                                        </span>
                                    </li>
                                </ul>
                                <div class="pg-portfolio-details-social">
                                    <h6>SHARE BY :</h6>
                                    <div class="social-icons social-icons-rounded">
                                        
                                         
                                    </div>-->
                                </div>
                            </div>
        <!--// Portfolio Details Area -->
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
<!--
        <!-- Portfolio Related Projects 
        <div class="pg-portfolio-related-projcts bg-white section-padding-bottom-lg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title-4 text-center">
                            <h2>Outras Ofertas</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    <div class="col-lg-4 col-md-6 col-12">
                            <!-- Signle Project 
                        <div class="related-portfolio text-center">
                            <a href="portfolio-details.html" class="related-portfolio-thumb">
                                <img src="images/leandro/livro.png" alt="Related Project">
                            </a>
                            <div class="related-portfolio-content">
                            <h5><a href="#">Explorers Game</a></h5>
                                
                            </div>
                        </div>    
                        <!--// Signle Project 
                    </div>

                    <div class="col-lg-4 col-md-6 col-12">
                            <!-- Signle Project 
                        <div class="related-portfolio text-center">
                            <a href="portfolio-details.html" class="related-portfolio-thumb">
                                <img src="images/leandro/livro.png" alt="Related Project">
                            </a>
                            <div class="related-portfolio-content">
                            <h5><a href="#">Explorers Game</a></h5>

                            </div>
                        </div>    
                        <!--// Signle Project 
                    </div>

                    <div class="col-lg-4 col-md-6 col-12">
                            <!-- Signle Project 
                        <div class="related-portfolio text-center">
                            <a href="portfolio-details.html" class="related-portfolio-thumb">
                                <img src="images/leandro/livro.png" alt="Related Project">
                            </a>
                            <div class="related-portfolio-content">
                            <h5><a href="#">Explorers Game</a></h5>

                            </div>
                        </div>    
                        <!--// Signle Project 
                    </div>

                <div class="col-lg-4 col-md-6 col-12">
                            <!-- Signle Project 
                        <div class="related-portfolio text-center">
                            <a href="portfolio-details.html" class="related-portfolio-thumb">
                                <img src="images/leandro/livro.png" alt="Related Project">
                            </a>
                            <div class="related-portfolio-content">
                            <h5><a href="#">Explorers Game</a></h5>

                            </div>
                        </div>    
                        <!--// Signle Project 
                    </div>

                    <div class="col-lg-4 col-md-6 col-12">
                            <!-- Signle Project 
                        <div class="related-portfolio text-center">
                            <a href="portfolio-details.html" class="related-portfolio-thumb">
                                <img src="images/leandro/livro.png" alt="Related Project">
                            </a>
                            <div class="related-portfolio-content">
                            <h5><a href="#">Explorers Game</a></h5>

                            </div>
                        </div>    
                        <!--// Signle Project 
                    </div>

                    <div class="col-lg-4 col-md-6 col-12">
                            <!-- Signle Project 
                        <div class="related-portfolio text-center">
                            <a href="portfolio-details.html" class="related-portfolio-thumb">
                                <img src="images/leandro/livro.png" alt="Related Project">
                            </a>
                            <div class="related-portfolio-content">
                            <h5><a href="#">Explorers Game</a></h5>

                            </div>
                        </div>    
                        <!--// Signle Project 
                    </div>
                </div>
            </div>
        </div>
        <!--// Portfolio Related Projects 
    -->
    </main>
    <!--// Start Page Content -->
</div>
<!-- //Main wrapper -->
@endsection