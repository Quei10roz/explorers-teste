
@extends('main')
<!-- CSS Style -->
<link rel="stylesheet" href="css/estilo2.css">

@section('content') 
<!-- Main wrapper -->
<div class="wrapper" id="wrapper">

    <!-- Breadcrumb Area -->
    <div class="banner fullscreen jarallax" data-black-overlay="4"
    style="background-image: url(images/leandro/oferta.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cr-breadcrumb text-center">
                        
                        <h1>QUEM SOMOS</h1>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Breadcrumb Area -->

</div>
@endsection