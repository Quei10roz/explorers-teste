@extends('main')

@section('title', '| Membros')

@section('content')
<!-- Breadcrumb Area -->
    <div class="banner fullscreen jarallax" data-black-overlay="4"
    style="background-image: url(images/leandro/1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cr-breadcrumb text-center">
                        <h6>conheça os nossos parceiros nessa jornada</h6>
                        <h1>MEMBROS DO EXPLORERS</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--// Breadcrumb Area -->

    <!-- Start Page Content -->
    <main class="page-content">
        <!-- Team Member -->
        <section class="cr-section team-member-area section-padding-xlg bg-white">
            <div class="container">

                <!--<div class="row">
                    <div class="col-lg-10 offset-lg-1 col-12">
                        <div class="section-title section-title-2 text-center">
                            <h6>our dedicated team</h6>
                            <h2>Team Member</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eismod tempor incididunt ut labore et dd quia consequuntur
                                magnolores eos qui ratione voluptatem sequ.</p>
                        </div>
                    </div>
                </div>-->

                <div class="row justify-content-center team-gallery">
                        
                        @foreach($users as $user)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="team">
                            <!-- Single Team -->                            
                                <div class="team-thumb text-center">
                                    @if ($user->image != null)
                                        <img src="{{'/fotos_users/'.$user->image}}" class="text-center" alt="User Image"/>
                                    @else
                                    <!-- Foto Padrao -->
                                        <img src="/fotos_users/padrao.jpg" alt="logo explorers"/>
                                    @endif
                                    <div class="social-icons social-icons-rounded">
                                        <ul>
                                            @if($user->facebook != null)
                                                <li class="facebook">
                                                    <a href="{{$user->facebook}}">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            @if($user->twitter != null)
                                                <li class="twitter">
                                                    <a href="{{$user->twitter}}">
                                                        <i class="fa fa-twitter"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            @if($user->linkedin != null)
                                                <li class="linkedin">
                                                    <a href="{{$user->linkedin}}">
                                                        <i class="fa fa-linkedin"></i>
                                                    </a>
                                                </li>
                                            @endif
                                            @if($user->instagram != null)
                                                <li class="instagram">
                                                    <a href="{{$user->instagram}}">
                                                        <i class="fa fa-instagram"></i>
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="team-content text-center">
                                    <h5>{{ $user->name }}</h5>
                                    <h6>{{ $user->cidade }}</h6>
                                    <br>                                
                                    <div class="header-buttons">
                                        <a href="/exploradores/{{$user->slug}}" class="cr-btn cr-btn-small cr-btn-blue cr-btn-transparent">
                                            <span>SAIBA MAIS</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--// Single Team -->
                        </div>
                        @endforeach
                </div>
            </div>
        </section>
        <!--// Team Member -->
    </main>
    <!--// Start Page Content -->   
</div>
<!-- //Main wrapper -->
@endsection
