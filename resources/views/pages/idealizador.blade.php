@extends('main')

@section('title', '| Idealizador')

<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/vendor/jquery-3.2.1.min.js"></script>
<!-- Custom Theme files -->
<link href="css/estilo.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Preface Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<!-- webfonts -->
<link href='//fonts.googleapis.com/css?family=Asap:400,700,400italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

<!-- webfonts -->
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>

@section('content')
		<!-- container -->
			<!-- header -->
			<div id="home" class="header">
				<div class="container">
				<!-- top-hedader -->
				<div class="top-header">
					<!-- /logo -->
					<!--top-nav---->
					<div class="top-nav">
					    <div class="navigation">
				                <br>
						            <br>
								<br>
					         <div class="clearfix"></div>
                        </div>
				    <!-- /top-hedader -->
				    </div>

			<div class="banner-info">
				<div class="col-md-7">
					<div class="logo"><br><br>
                        <div class="form-group mt-4 ml-4">
						<h3><span>L</span>EANDRO JESUS</h3>
                        </div>
                        <p class="text-white text-left">Não acredito mais em currículos como forma de nos apresentarmos e falarmos sobre quem somos. Penso que diplomas e títulos servem mais para alimentar nosso ego e transmitir a imagem de que somos seres superiores. Acho até que nos afastam, ao invés de nos aproximar, de quem está buscando nos conhecer.</p>
                        <p class="text-white text-left">Dito isso, reservei esse espaço para falar um pouco sobre minhas crenças e motivações atuais.</p>
                        <p class="text-white text-justify">Considero-me alguém – possivelmente mais um dentre muitos, nesse momento ímpar – numa jornada de transformação individual. Uma jornada incrível, que está mudando minha maneira de enxergar o mundo, ajudando-me a redefinir um propósito de vida e entender como quero contribuir com quem está ao meu redor.</p>
                        <p class="text-white text-justify">Quando falo dessa transformação, não me refiro aqui apenas à minha atuação profissional – embora os que me acompanham saibam que, há pouco tempo, decidi me afastar da empresa que fundei, a EloGroup, em busca de novos sonhos.</p>
                        <p class="text-white text-justify">Refiro-me também à transformação em aspectos da minha vida pessoal, desde meus hábitos alimentares e mudança de domicílio até uma busca por auto-conhecimento e equilíbrio na relação família e trabalho. Uma transformação que está me permitindo valorizar coisas simples, como as relações humanas, e desapegar de status e realizações materiais (aliás, me permitindo até fazer piada de mim mesmo e da imagem séria que procurei construir em minha carreira).</p>
                        <p class="text-white text-left">Nesse sentido, defino-me como um explorador, alguém inconformado com o status quo e que busca empreender novos caminhos visando à construção de uma sociedade melhor. Alguém que acredita que podemos viver melhor numa nova economia, mais colaborativa, abundante e preocupada com o bem-estar coletivo.</p>

                         <div class="col-md-8">
                            <p class="text-white text-left">Um grande aprendizado nessa jornada foi compreender que só estamos em sintonia, de fato, quando alinhamos o que pensamos, o que falamos e como agimos. Tenho procurado seguir essa máxima à risca, agindo na direção do mundo que desejo e que acredito estar surgindo.</p>
                            <p class="text-white text-justify">No fundo, penso que, dessa forma, talvez eu possa servir de exemplo e inspiração para pessoas que me acompanham. O que me motiva, nesse momento, é ajudar a despertar a consciência das pessoas para esse momento de transição da sociedade, para que possam também passar por suas jornadas particulares de transformação. Não tenho dúvidas de que esse é um processo pelo qual cada um de nós precisa passar, por conta própria, para se desprender das amarras existentes em nossas vidas.</p>
                            <p class="text-white text-justify">Outras informações importantes sobre mim: sou carioca, porém introspectivo. Sou casado com a Larissa, minha grande companheira nessa jornada de transformação, e pai do Tomás e da Mila. Atualmente moro em São José dos Campos-SP, mas me considero um cidadão do mundo.</p>
                        </div>

						<!--<h6 class=" texto1 text-white text-justify mt-4 ml-4"> Autor do livro “Exploradores de um mundo em transformação: conduzindo organizações na travessia para uma nova era”. Mestre em Engenharia de Produção pela Universidade Federal do Rio de Janeiro (UFRJ), com estudos na Carnegie Mellon University (EUA), já atuou como professor de pós-graduação em instituições como FGV, PUC e UFRJ. Empreendedor e cofundador da EloGroup, empresa de consultoria e tecnologia com atuação em diversas capitais do país, reconhecida por seguidos anos como umas das melhores empresas para se trabalhar no Brasil e na América Latina pelo instituto Great Place to Work. Ao longo dos últimos anos, tornou-se mentor e investidor em startups. Criou a comunidade Business Transformation (BT) e atuou como Conselheiro no Instituto Capitalismo Consciente Brasil.</h6>-->
                    </div>
				</div>

				<div class="col-md-5 header-left">
					<img src="images/leandro/membros/leandro.png" alt="">
                 </div>

				<div class="clearfix"> </div>
		      </div>
            </div>
            
            <!--<span class="border two"></span>-->
            <div class="col-md-3 col-sm-6  ">
                            <img class="pirata3 mt-5" src="images/leandro/pirata3.png">
                            <em class="text-white ">“A transformação começa com cada um de nós. Seja a mudança que você quer ver no mundo”</em>
                        </div>


                        <div class="col-md-4 col-sm-4">
                            <img class="leandro4" src="images/leandro/leandro4.jpg">
                        </div>
		</div>
	</div>


        <!-- services -->
        <div id="services" class="services">
            <div class="container">
                <div class="service-head one text-center ">
                    <h3 class="mt-5">QUEM SOU <span>EU?</span>
                        <h4 >OLÁ, EXPLORADORES!</h4>

                        

                        <p class="text-white text-left col-md-12"> Caso você se interesse, falo um pouco no vídeo abaixo sobre minha trajetória de transformação pessoal, junto com minha esposa Larissa, para o Congresso Família no Mundo, em março de 2017.</p>
                        <p><iframe class=' youtube-player' type='text/html' width='640' height='390' src='http://www.youtube.com/embed/Ugld6jiRqJw?version=3&#038;rel=1&#038;fs=1&#038;autohide=2&#038;showsearch=0&#038;showinfo=1&#038;iv_load_policy=1&#038;wmode=transparent' allowfullscreen='true' style='border:0;'></iframe></p>
                </div>
            </div>
        </div>

        <!-- /services -->


		<!-- portfolio -->
        <!-- top-grids -->
        <div class="blog" id="blogs">
            <div class="news-grid w3l-agile">
                <div class="portfolio" id="port">
				    <div class="mt-3 service-head text-center">
						<h4>FICOU CURIOSO?</h4>
						<h3>FALE <span>COMIGO</span></h3>
                       <p>Preencha o formulário abaixo para entrar em contato com o Leandro Jesus.</p>
						<span class="border"></span>
					</div>

			<div class="portfolio-grids">
				<script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
				<script type="text/javascript">
									$(document).ready(function () {
										$('#horizontalTab').easyResponsiveTabs({
											type: 'default', //Types: default, vertical, accordion
											width: 'auto', //auto or any width like 600px
											fit: true   // 100% fit in a container
										});
									});
				</script>
	        </div>
	<!-- //portfolio -->
					    <div class="col-md-6 mb-5 news-img">
						    <a  data-toggle="modal" data-target="#myModal1">
                                  <div class="promocontent promocontent-7 contact-me-content">
                                      <div class="pirate_forms_wrap">

                                          <form method="post" id="c828648fa3" enctype="application/x-www-form-urlencoded" class="pirate_forms  pirate_forms_ajax-on form_honeypot-on pirate_forms_from_widget-on wordpress-nonce-on pirate-forms-contact-name-on pirate-forms-contact-email-on pirate-forms-contact-message-on pirate-forms-contact-submit-on pirate-forms-contact-referrer-on pirate_forms_from_form-on">
                                              <div class="pirate-forms-fields-container">
                                                  <div class="pirate_forms_three_inputs_wrap">
                                                      <div class="col-lg-6 col-sm-4 form_field_wrap">
                                                          <input type="text" id="pirate-forms-contact-name" name="pirate-forms-contact-name" class="form-control input" placeholder="Seu nome" required oninvalid="this.setCustomValidity('Preencha seu nome')" onchange="this.setCustomValidity('')" value="">
                                                      </div>

                                                      <div class=" form-group col-lg-6 col-sm-4 form_field_wrap">
                                                          <input type="email" id="pirate-forms-contact-email" name="pirate-forms-contact-email" class="form-control input" placeholder="Seu email" required oninvalid="this.setCustomValidity('Preencha com um e-mail válido')" onchange="this.setCustomValidity('')" value="">
                                                      </div>
                                                  </div>

                                                  <div class=" form-group col-lg-12 col-sm-12 form_field_wrap">
                                                      <textarea rows=5 cols=30 id="pirate-forms-contact-message" name="pirate-forms-contact-message" class="form-control input" placeholder="Mensagem" required oninvalid="this.setCustomValidity('Escreva um comentário ou questão')" onchange="this.setCustomValidity('')"></textarea>
                                                  </div>

                                                  <div class="pirate-forms-footer col-md-offset-11 ">
                                                      <div class="col-xs-12 form_field_wrap contact_submit_wrap"><button type="submit" id="pirate-forms-contact-submit" name="pirate-forms-contact-submit" class="btn btn-primary custom-button red-btn pirate-forms-submit-button" placeholder="" >Enviar</button></div>
                                                  </div>
                                              </div>

                                              <input type="hidden" id="pirate_forms_ajax" name="pirate_forms_ajax" class="" placeholder=""  value="0">
                                              <div class="form_field_wrap hidden" style="display: none">
                                                  <input type="text" id="form_honeypot" name="honeypot" class="" placeholder=""  value="">
                                              </div>

                                              <input type="hidden" id="pirate_forms_from_widget" name="pirate_forms_from_widget" class="" placeholder=""  value="0">
                                              <input type="hidden" id="wordpress-nonce" name="wordpress-nonce" class="" placeholder=""  value="24399d729f">
                                              <input type="hidden" id="pirate-forms-contact-referrer" name="pirate-forms-contact-referrer" class="" placeholder=""  value="http://explorers.digital/">
                                              <input type="hidden" id="pirate_forms_from_form" name="pirate_forms_from_form" class="" placeholder=""  value="c828648fa3">
                                          </form>

                                          <div class="pirate_forms_clearfix"></div>
                                      </div>

                                      <p class="form-message"></p>

                                  </div>
                                </a>
						    </div>
						<div class="clearfix"></div>
					 </div>
                </div>
            </div>
				<!-- top-grids -->
	<script src="js/bootstrap.js"></script>

@endsection

