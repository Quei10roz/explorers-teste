<!doctype html>
<html class="no-js" lang="zxx">
	@include('partials._head')
<body>	 

	<!-- Add your site or application content here -->
	<div class="fakeloader"></div>
	<!-- Main wrapper -->
	<div class="wrapper" id="wrapper">


	@include('partials._nav')

	@yield('content')

	@include('partials._footer')

	@include('partials._scripts')


	</div>
	<!-- //Main wrapper -->
	
</body>

</html>