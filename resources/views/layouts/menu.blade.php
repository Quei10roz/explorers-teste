<li class="{{ Request::is('users/'.Auth::id().'/edit') ? 'active' : '' }}">
    <a href="{!! route('users.edit', [Auth::id()]) !!}"><i class="fa fa-user"></i><span>Atualizar Perfil</span></a>
</li>

<li class="{{ Request::is('SeusEventos') ? 'active' : '' }}">
    <a href="{!! route('seuseventos') !!}"><i class="fa fa-calendar-check-o"></i><span>Seus Eventos</span></a>
</li>

<li class="{{ Request::is('SuasAvaliacoes') ? 'active' : '' }}">
    <a href="{!! route('suasavaliacoes') !!}"><i class="fa fa-star"></i><span>Suas Avaliacoes</span></a>
</li>

<li class="{{ Request::is('SeusPosts') ? 'active' : '' }}">
    <a href="{!! route('seusposts') !!}"><i class="fa fa-newspaper-o"></i><span>Seus Posts</span></a>
</li>

<li class="{{ Request::is('SeusComentarios*') ? 'active' : '' }}">
    <a href="{!! route('seuscomentarios') !!}"><i class="fa fa-comments"></i><span>Seus Comentarios</span></a>
</li>

<li class="{{ Request::is('SeusOfertas*') ? 'active' : '' }}">
    <a href="{!! route('ofertas.cadastro') !!}"><i class="fa fa-shopping-cart"></i><span>Cadastro de Ofertas</span></a>
</li>

@if(Auth::user()->level_acl == 2)

    <li class="treeview">
        <a href="#">
            <i class="fa fa-rocket"></i><span>Área de Administrador</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{!! route('users.index') !!}"><i class="fa fa-users"></i>Membros</a></li>
            <li><a href="{!! route('eventos.index') !!}"><i class="fa fa-map"></i>Eventos</a></li>
            <li><a href="{!! route('evaluations.index') !!}"><i class="fa fa-star"></i>Avaliacoes</a></li>
            <li><a href="{!! route('tags.index') !!}"><i class="fa fa-tag"></i>Tags</a></li>
            <li><a href="{!! route('posts.index') !!}"><i class="fa fa-newspaper-o"></i>Posts</a></li>
            <li><a href="{!! route('comentarios.index') !!}"><i class="fa fa-comments"></i>Comentarios</a></li>
            <li><a href="{!! route('ofertas.index') !!}"><i class="fa fa-shopping-cart"></i>Ofertas</a></li>
            
        </ul>
    </li>
@endif
<!--
<li>
    <a href="{!! url('/logout') !!}" class="fa fa-sign-out"
        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
        Logout
    </a>
    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</li>
-->
