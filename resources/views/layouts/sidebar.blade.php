<aside class="main-sidebar" id="sidebar-wrapper">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">   
                <!-- trocar por asset('fotos_usuarios/'.Auth::user()->image) caso precise -->           
                @if (Auth::user()->image != '')
                    <img src="{{'/fotos_users/'.Auth::user()->image}}" class="img-circle" alt="User Image"/>
                @else
                 <!-- Foto Padrao -->
                    <img src="/fotos_users/padrao.jpg" alt="logo explorers" class="img-circle">
                @endif
            </div>
            <div class="pull-left info">
                @if (Auth::guest())
                <p>InfyOm</p>
                @else
                    <p>{{ Auth::user()->name}}</p>
                @endif
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->

        <ul class="sidebar-menu" data-widget="tree">
            @include('layouts.menu')
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>