<table class="table table-responsive" id="ofertas-table">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        @foreach($ofertas as $ofertas)
            <tr>
                <td>{!! $ofertas->nome !!}</td>
                <td>{!! ($ofertas->aprovado == 0) ? ('Em analise') : ($ofertas->aprovado == 1 ? 'Aprovado' : 'Negado') !!}</td>
                <td>
                    {!! Form::open(['route' => ['ofertas.destroy', $ofertas->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <!-- Ver dados dessa oferta -->
                        <a href="{!! route('ofertas.show', [$ofertas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <!-- Editar oferta -->
                        <a href="{!! route('ofertas.edit', [$ofertas->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        <!-- Deletar oferta -->
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza que quer fazer isso?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>