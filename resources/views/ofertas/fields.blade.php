<!-- Nome Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nome', 'Nome:') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Perfil Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_perfil', 'Icone Oferta:') !!}
    {!! Form::file('image_perfil') !!}
</div>

<!-- Image Banner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_banner', 'Banner da Oferta:') !!}
    {!! Form::file('image_banner') !!}
</div>        
    
<!-- Descricao Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descricao', 'Descrição:') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ofertas.index') !!}" class="btn btn-default">Cancelar</a>
</div>
