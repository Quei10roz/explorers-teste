@extends('layouts.app')

@section('title', '| Criando Oferta')

@section('content')
    <section class="content-header">
        <h1>
            Ofertas
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ofertas.store', 'enctype' => 'multipart/form-data']) !!}

                        @include('ofertas.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
