<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{!! $ofertas->nome !!}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('slug', 'link:') !!}
    <p>http://explorers.digital/{!! $ofertas->slug !!}</p>
</div>    

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    <p>{!! $ofertas->descricao !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! date('j M Y', strtotime($ofertas->created_at)) !!}</p>
</div>

