<table class="table table-responsive" id="ofertas-table">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Estado</th>
            <th>Cidade</th>
        </tr>
    </thead>
    <tbody>
    @foreach($membros as $membros)
        @if($membros != null)
            <tr>
                <td>{!! $membros->name !!}</td>
                <td>{!! $membros->estado !!}</td>
                <td>{!! $membros->cidade !!}</td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>