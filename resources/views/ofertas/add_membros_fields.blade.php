
@foreach (array_combine($membros_list, $checked) as $membros => $checked)

<div class="form-group col-sm-12"> 
    {!! Form::checkbox('ids[]', $membros, $checked, ['class' => 'icheckbox_flat-blue']) !!}
    {!! Form::label('nome', App\User::where('id', $membros)->first()->name) !!}
</div>

@endforeach

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Atualizar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ofertas.membros',[$oferta->id]) !!}" class="btn btn-default">Cancelar</a>
</div>
