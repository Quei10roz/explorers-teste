@extends('layouts.app')

@section('title', '| Membros nessa Oferta')

@section('content')
    <section class="content-header">
    <h1 class="pull-left">Membros em {{$oferta->nome}}</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('ofertas.add_membros',[$oferta->id]) !!}">Adicionar Membros</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        
        <div class="box box-primary">
            <hr>
            <div class="col-md-12">
                {!! Form::open(['route' => ['buscaUser_Oferta', $oferta->id], 'method' => 'get']) !!}                  
                    <div class="input-group input-group-sm">
                        {!! Form::text('pesquisa', null, ['class' => 'form-control']) !!}
                        <span class="input-group-btn">
                            {!! Form::submit('Pesquisar',['class'=>'btn btn-primary btn-flat']) !!}
                        </span>
                    </div>
                    <hr>
                {!! Form::close() !!}   
                <!-- Submit Field -->
            </div>
            <div class="box-body">
                    @include('ofertas.table_membros')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

