@extends('layouts.app')

@section('title', '| Atualizando Oferta')

@section('content')
    <section class="content-header">
        <h1>
            Ofertas
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ofertas, ['route' => ['ofertas.update', $ofertas->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('ofertas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection