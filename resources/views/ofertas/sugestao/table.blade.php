<table class="table table-responsive" id="ofertas-table">
        <thead>
            <tr>
                <th>Autor</th>
                <th>Nome</th>
            </tr>
        </thead>
        <tbody>
        @foreach($ofertas as $ofertas)
            <tr>
                <td>{{ substr(\App\User::find($ofertas->user_id)->name, 0, 30) }}{{ strlen(\App\User::find($ofertas->user_id)->name) > 30 ? "..." : "" }}</td>
                <td>{!! $ofertas->nome !!}</td>
                <td>                    
                    <a href="{!! route('ofertas.show', [$ofertas->id]) !!}" class='btn btn-info'>Ver Mais</a>
                    <div class='btn-group'>                        
                        <a href="{{ route('ofertas.aprovar', ['id' => $ofertas->id]) }}" class='btn btn-success'>Aprovar</a>
                        <a href="{{ route('ofertas.negar', ['id' => $ofertas->id]) }}" class='btn btn-danger'>Recusar</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>