
<table class="table table-responsive" id="eventos-table">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Cidade</th>
            <th>Data</th>
            <th>Oferta</th>
            <th>Criador</th>
            <th>Privado</th>
        </tr>
    </thead>
    <tbody>
    @foreach($eventos as $evento)
        <tr>
            <td>{{ substr($evento->titulo, 0, 40) }}{{ strlen($evento->titulo) > 40 ? "..." : "" }}</td>
            <td>{{ substr($evento->cidade, 0, 9) }}{{ strlen($evento->cidade) > 9 ? "..." : "" }}</td>
            <td>{!! date('j M Y', strtotime($evento->data)) !!}</td>
            <td>{!! App\Oferta::where('id', $evento->oferta_id)->first()->nome !!}</td>
            <td>{!! App\User::where('id', $evento->user_id)->first()->name !!}</td>
            <td>
                {!! $evento->privado ? 'SIM' : 'NÃO' !!}
            </td>
            <td>
                {!! Form::open(['route' => ['eventos.destroy', $evento->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <!-- Ver Membros nessa oferta -->
                    <a href="{!! route('eventos.avaliacoes', [$evento->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-star"></i></a>

                    <a href="{!! route('eventos.show', [$evento->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>

                    <a href="{!! route('eventos.edit', [$evento->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>

                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza que quer fazer isso?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">
        {!! $eventos->links() !!}
    </div>
