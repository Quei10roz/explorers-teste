
<!-- Titulo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('titulo', 'Título:') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<<<<<<< HEAD
<!-- privado Field -->
<div class="form-group col-sm-12">
    {!! Form::label('privado', 'Privado:')!!}
    {!! Form::checkbox('privado', true, false, ['class' => 'icheckbox_flat-blue']) !!}
</div>

<!-- numero Field -->
<div class="form-group col-sm-6">
    {!! Form::label('numero_participantes', 'Número de Participates:') !!}
    {!! Form::number('numero_participantes', null, ['class' => 'form-control']) !!}
</div>

<!-- tipo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_eventos', 'Tipo de Evento:') !!}
    {!! Form::select('tipo_eventos', ['Acadêmico','Empresarial'], null, ['class' => 'form-control']) !!}
</div>
        
<!-- Cidade Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cidade', 'Cidade:') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>
=======

>>>>>>> 7997e5a279e3410306af3142c78d57c8e1324301

<!-- Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data', 'Data:') !!}
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
</div>

<!-- Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('link', 'Link:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!-- Oferta Field -->
<div class="form-group col-sm-6">
    {!! Form::label('oferta', 'Oferta:') !!}
    {!! Form::select('oferta', $ofertas_list, null, ['class' => 'form-control']) !!}
</div>
    
<!-- Descricao Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descricao', 'Descrição:') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('eventos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
