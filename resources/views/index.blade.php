@extends('main')

@section('title',' Game!')

@section('content')    
    <!-- Banner Area -->
    <div class="banner-area ">
        <!-- Single Banner -->
        <div class="banner fullscreen jarallax" data-black-overlay="7" data-speed="0.7" style="background-image: url(images/leandro/banner.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-md-12 offset-lg-1 col-12 offset-0">
                        <div class="banner-content text-center cr-tilter">
                                <h1>Seja 
                                        <span>você também</span> um explorador do novo mundo!</h1>
                        </div>
                    </div>
                </div>                    
            </div>
        </div>
        <!--// Single Banner -->
    </div>
    <!--// Banner Area -->

    <!-- Start Page Content -->
    <main class="page-content">
       

        <!-- Features Area -->
        <section class="cr-section features-area section-padding-top-xlg jarallax" data-speed="0.5" style="background-image: url(images/leandro/mapa.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="section-title section-title-2 text-center" >
                            	<img src="images/leandro/pergaminho.png"  style="height:850px;">				
                        </div>
                    </div>
                </div>
        <!--// Features Area -->		
                </div>
            </div>
   
        <!--// Countbox Area -->
        <!-- Features Area -->
       <!-- <section class="cr-section features-area section-padding-top-xlg jarallax" data-speed="0.5" style="background-image: url(images/leandro/10.jpg);">
                <div class="container" >
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <div class="section-title section-title-2 text-center"> 
                                <h2>Flexible</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eismod tempor incididunt ut labore et dd quia consequuntur
                                    magnolores eos qui ratione voluptatem sequ.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="features-area-inner">
                    <div class="features-area-thumb">
                        <img src="images/feature/feature-thumb-1.png" alt="feature thumb">
                    </div>
                    <div class="features-area-features">
                        <div class="row">

                            <div class="col-lg-6 col-md-6">
                                <!-- Single Feature 
                                <div class="feature">
                                    <h6>Pixel Perfect</h6>
                                    <p>Lorem ipsum dolor sit amet, consadipisicing elit, sed do eiusmod tempo.</p>
                                </div>
                                <!--// Single Feature 
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <!-- Single Feature 
                                <div class="feature">
                                    <h6>Support</h6>
                                    <p>Lorem ipsum dolor sit amet, consadipisicing elit, sed do eiusmod tempo.</p>
                                </div>
                                <!--// Single Feature 
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <!-- Single Feature 
                                <div class="feature">
                                    <h6>Easy to Customize For All Device</h6>
                                    <p>Lorem ipsum dolor sit amet, consadipisicing elit, sed do eiusmod tempo.</p>
                                </div>
                                <!--// Single Feature 
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <!-- Single Feature 
                                <div class="feature">
                                    <h6>Retina Ready</h6>
                                    <p>Lorem ipsum dolor sit amet, consadipisicing elit, sed do eiusmod tempo.</p>
                                </div>
                                <!--// Single Feature 
                            </div>

                        </div>
                    </div>
                </div>
            </section>-->
            <!--// Features Area -->
        <!-- Why Choose Us -->
       <!-- <div class="cr-section cr-choose-us bg-grey">
            <div class="row no-gutters align-items-center">
                <div class="col-xl-6 col-12 order-2 order-xl-1">
                    <div class="promocontent promocontent-2 about-content">
                        <div class="section-title section-title-2 text-left color-dark">
                            <h6>Why Choose Us</h6>
                            <h2>Check out Our Videos </h2>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu mod tempor incididunt ut labore et dolore magna
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi quis nostrud exercitation ut aliquip</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiu mod tempor incididunt ut labore et dolore magna
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut nostrud exercitation ullamco aliquip.</p>
                        <a href="about-us.html" class="cr-btn">
                            <span>Buy Now</span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-6 col-12 order-1 order-xl-2">
                    <div class="videobox" data-black-overlay="2">
                        <img src="images/promo-image/promo-image-3.jpg" alt="promo image">
                        <a href="https://vimeo.com/143996226" class="cr-video-btn video-popup-trigger">
                            <i class="fa fa-play"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--// Why Choose Us -->
        </section>        
<br>
<br>
<br>
        <!-- Blog Area -->
			<section class="cr-section blog-area section-padding-bottom-lg">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 offset-lg-3">
							<div class="section-title text-center">
                            <h5>Agenda</h5>
                            <h2>Fique por dentro dos nossos próximos eventos</h2>
							</div>
						</div>
					</div>
					<!-- Carrossel -->   
        <div class="cr-section bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="brand-logos slider-navigation-style-1"> 
								@foreach($eventos as $evento)
									<!-- ITEM -->
									<div class="single-brand-logo">
										<a href="#">
											<div class="service service-style-2">
												<div class="service-content">
													<img src="{{'/fotos_ofertas/'.App\Oferta::where('id', $evento->oferta_id)->first()->image_perfil}}" alt="">
													<p>
														{{ $evento->titulo }}
													</p>
													<h6>{{ $evento->cidade }}</h6>
													<p>{!! date('j M Y', strtotime($evento->data)) !!}</p>
												</div>
											</div>
										</a>
									</div> 
									<!-- ITEM -->   
								@endforeach                      
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<!-- Carrossel -->  
			<br>
			<br>
			<br>
<!--
					<div class="row blogs-grid blog-slider-active slider-navigation-style-1">
						<!-- Single Blog 
						@foreach($eventos as $evento)
						<div class="col-lg-4">
							<div class="blog-item">
								<div class="blog-thumb">
									<img src="{{'/fotos_ofertas/'.App\Oferta::where('id', $evento->oferta_id)->first()->image_perfil}}" alt="blog thumb">
								</div>
								<div class="blog-content">
								<ul class="blog-meta">
											<li>
												<a href="#"{{ $evento->cidade }}-{{ $evento->estado }}</a>
											</li>
										</ul>

										<h5 class="blog-title">
											<a href="#">{{ $evento->titulo }}</a>
										</h5>
								</div>
								
								<div class="blog-content-hover">
									<div class="blog-content-hover-inner">
										<div class="blog-info">
											<span class="date">{!! date('j', strtotime($evento->data)) !!}</span>
											<span class="month">{!! date('M', strtotime($evento->data)) !!}</span>
										</div>

										<ul class="blog-meta">
											<li>
												<a href="#">{{ $evento->cidade }}-{{ $evento->estado }}</a>
											</li>
										</ul>

										<h5 class="blog-title">
											<a href="#">{{ $evento->titulo }}</a>
										</h5>
										<h6>
											<a href="#">{{ App\Oferta::where('id', $evento->oferta_id)->first()->nome }}</a>
										</h6>
										<p>{{ substr($evento->descricao, 0, 30) }}{{ strlen($evento->descricao) > 30 ? "..." : "" }}</p>
										<a href="#" class="cr-btn cr-btn-white cr-btn-transparent cr-btn-small">
											<span>Saiba mais</span>
										</a>
									</div>
								</div>
							</div>
						</div>
						<!--// Single Blog 
						@endforeach
					</div>
				</div>
			</section>
			<!--// Blog Area -->

         <!-- Service Area 
         <div class="cr-section services-area section-padding-xlg bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="section-title section-title-2 text-center">
                            <h6>Agenda</h6>
                            <h2>Fique por dentro dos nossos próximos eventos</h2>
                        </div>
                    </div>
                </div>
        <!-- Carrossel  
        <div class="cr-section bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="brand-logos slider-navigation-style-1"> 
                                <!-- ITEM 
                                <div class="single-brand-logo">
                                    <a href="#">
                                        <div class="service service-style-2">
                                            <div class="service-content">
                                                <img src="images/leandro/logoe.png" alt="">
                                                <h6>SALVADOR</h6>
                                                <p>19 OUTUBRO, 2018</p>
                                            </div>
                                        </div>
                                    </a>
                                </div> 
                                <!-- ITEM --> 
                                <!-- ITEM 
                                <div class="single-brand-logo">
                                        <a href="#">
                                            <div class="service service-style-2">
                                                <div class="service-content">
                                                    <img src="images/leandro/logoe.png" alt="">
                                                    <h6>SÃO PAULO</h6>
                                                    <p>10 OUTUBRO, 2018</p>
                                                </div>
                                            </div>
                                        </a>
                                </div> 
                                <!-- ITEM --> 
                                <!-- ITEM 
                                <div class="single-brand-logo">
                                        <a href="#">
                                            <div class="service service-style-2">
                                                <div class="service-content">
                                                    <img src="images/leandro/logoe.png" alt="">
                                                    <h6>BELO HORIZONTE</h6>
                                                    <p>20 AGOSTO, 2018</p>
                                                </div>
                                            </div>
                                        </a>
                                </div> 
                                <!-- ITEM --> 
                                <!-- ITEM 
                                <div class="single-brand-logo">
                                        <a href="#">
                                            <div class="service service-style-2">
                                                <div class="service-content">
                                                    <img src="images/leandro/logoe.png" alt="">
                                                    <h6>CURITIBA</h6>
                                                    <p>30 SETEMBRO, 2018</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div> 
                                    <!-- ITEM --> 
                                    <!-- ITEM 
                                    <div class="single-brand-logo">
                                            <a href="#">
                                                <div class="service service-style-2">
                                                    <div class="service-content">
                                                        <img src="images/leandro/logoe.png" alt="">
                                                        <h6>PORTO ALEGRE</h6>
                                                        <p>25 DEZEMBRO, 2018</p>
                                                    </div>
                                                </div>
                                            </a>
                                    </div> 
                                    <!-- ITEM --> 
                                    <!-- ITEM 
                                    <div class="single-brand-logo">
                                            <a href="#">
                                                <div class="service service-style-2">
                                                    <div class="service-content">
                                                        <img src="images/leandro/logoe.png" alt="">
                                                        <h6>PALMAS</h6>
                                                        <p>24 NOVEMBRO, 2018</p>
                                                    </div>
                                                </div>
                                            </a>
                                    </div> 
                                    <!-- ITEM                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carrossel   
            </div>
        </div>
        <!--// Service Area -->
                    </div>
                </div>

            </div>
        </section>
        <br>
        <!--// Testimonial Area -->
    </main>
    <!--// Start Page Content -->    
@endsection