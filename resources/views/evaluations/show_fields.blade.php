
<!-- Nota Field -->
<div class="form-group">
    {!! Form::label('nota', 'Nota:') !!}
    <p>{!! $evaluations->nota !!}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{!! $evaluations->nome !!}</p>
</div>

<!-- Conteudo Field -->
<div class="form-group">
    {!! Form::label('conteudo', 'Conteudo:') !!}
    <p>{!! $evaluations->conteudo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{!! date('j M Y', strtotime($evaluations->created_at)) !!}</p>
</div>
