<table class="table table-responsive" id="evaluations-table">
    <thead>
        <tr>
            <th>Criador do Evento</th>
            <th>Evento</th>
            <th>Nota</th>
            <th>Conteudo</th>
            <th>Favorito</th>
        </tr>
    </thead>
    <tbody>
    @foreach($evaluations as $evaluation)
        <tr>
            <td>{{ substr(App\User::find($evaluation->user_id)->name, 0, 20) }}{{ strlen(App\User::find($evaluation->user_id)->name) > 20 ? "..." : "" }}</td>      
            <td>{!! App\Evento::find($evaluation->evento_id)->titulo !!}</td>
            <td>{!! $evaluation->nota !!}</td>
            <td>{{ substr($evaluation->conteudo, 0, 20) }}{{ strlen($evaluation->conteudo) > 20 ? "..." : "" }}</td>
            <td>{{ $evaluation->favorito? 'sim' : 'não' }}</td>
            <td>
                {!! Form::open(['route' => ['evaluations.destroy', $evaluation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('evaluations.favorito', [$evaluation->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-star"></i></a>
                    <a href="{!! route('evaluations.show', [$evaluation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza que quer fazer isso?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="text-center">
    {!! $evaluations->links() !!}
</div>