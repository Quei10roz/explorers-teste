@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Evaluations
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($evaluations, ['route' => ['evaluations.update', $evaluations->id], 'method' => 'patch']) !!}

                        @include('evaluations.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection