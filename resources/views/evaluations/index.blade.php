@extends('layouts.app')

@section('title', ' | Avaliações')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Avaliações</h1>
        <h1 class="pull-right">
           <a class="btn btn-danger pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('evaluations.create') !!}">EXCLUI ESSE BOTAO DPS</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('evaluations.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

