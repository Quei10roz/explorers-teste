@extends('layouts.app')

@section('title', ' | Avaliação')

@section('content')
    <section class="content-header">
        <h1>
            Avaliações
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('evaluations.show_fields')
                    <a href="{!! route('evaluations.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
