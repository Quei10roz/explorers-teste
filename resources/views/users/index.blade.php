@extends('layouts.app')

@section('title', '| Lista de Membros')

@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">

        jQuery(function($) {
            var user = $("#user");

            user.keyup(function () {

                $.ajax({
                    url: "/typeahead_user",
                    method: "GET",
                    data: {user: user.val()},
                    success: function (data) {

                        var valor1 = data[0] >= 1 ? data[1] : '';
                        var valor2 = data[0] >= 2 ? data[2] : '';
                        var valor3 = data[0] >= 3 ? data[3] : '';
                        var valor4 = data[0] >= 4 ? data[4] : '';
                        var valor5 = data[0] >= 5 ? data[5] : '';
                        var valor6 = data[0] >= 6 ? data[6] : '';
                        var availableTags = [valor1, valor2, valor3, valor4, valor5, valor6];

                        user.autocomplete({
                            source: availableTags
                        });
                    },
                    error: function () {
                        alert('Erro na comunicação');
                    }
                });
            });


        });


    </script>
@endsection
@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Membros</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('users.create') !!}">Criar Membro</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        
        @include('flash::message')

        <div class="clearfix"></div>

        <div class="box box-primary">
            <hr>
            <div class="col-md-12">
                {!! Form::open(['route' => 'buscaUser', 'method' => 'get']) !!}                  
                    <div class="input-group input-group-sm">
                        <input class="form-control" value=""  id="user" name="user" type="text">
                        <span class="input-group-btn">
                            {!! Form::submit('Pesquisar',['class'=>'btn btn-primary btn-flat']) !!}
                        </span>
                    </div>
                    <hr>
                {!! Form::close() !!}   
                <!-- Submit Field -->
            </div>
            
            <div class="box-body">
                    @include('users.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

