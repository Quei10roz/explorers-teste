<table class="table table-responsive" id="users-table">
    <thead>
        <tr>
            <th></th>
            <th>Name</th>
            <th>Estado</th>
            <th>Cidade</th>
            <th>Reputação</th>
            <th>Admin</th>
        </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td class="user-header">
                @if ($user->image != null)
                    <img src="{{'/fotos_users/'.$user->image}}" class="img-circle" alt="User Image"  style="height: 25px;"/>
                @else
                <!-- Foto Padrao -->
                    <img src="/fotos_users/padrao.jpg" alt="logo explorers" class="img-circle" style="height: 25px;">
                @endif

            </td>
            <td>{{ substr($user->name, 0, 30) }}{{ strlen($user->name) > 30 ? "..." : "" }}</td>
            <td>{{ substr($user->estado, 0, 30) }}{{ strlen($user->estado) > 30 ? "..." : "" }}</td>
            <td>{{ substr($user->cidade, 0, 30) }}{{ strlen($user->cidade) > 30 ? "..." : "" }}</td>
            <td>{!! $user->reputacao !!}</td>
            <td>{!! $user->level_acl == 2 ? 'Sim' : 'Não' !!}</td>
            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Tem certeza que quer fazer isso?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach    
    </tbody>
</table>
<div class="text-center">
    {!! $users->links() !!}
</div>