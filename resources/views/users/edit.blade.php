@extends('layouts.app')

@section('title', '| Atualizando Membro')

@section('content')
    <section class="content-header">
        <h1>
            Editar Perfil
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       @include('flash::message')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($users, ['route' => ['users.update', $users->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('users.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection