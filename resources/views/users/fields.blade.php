<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Senha:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Password Confirmation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', 'Confirmação de Senha:') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

<!-- Facebook Field -->
<div class="form-group col-sm-6">
    {!! Form::label('facebook', 'Facebook Link (opcional):') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<!-- Instagram Field -->
<div class="form-group col-sm-6">
    {!! Form::label('instagram', 'Instagram Link (opcional):') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<!-- Twitter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('twitter', 'Twitter Link (opcional):') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>

<!-- Linkedin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('linkedin', 'Linkedin Link (opcional):') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::text('estado', null, ['class' => 'form-control']) !!}
</div>

<!-- Cidade Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cidade', 'Cidade:') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
       
    <div class="col-sm-4"></div>  
    <div class="col-sm-6">  

            {!! Form::label('image', 'Foto de perfil:') !!}
            @if ($user_flag == 2)
                <img src="{{'/fotos_users/'.$users->image}}" class="img-circle" alt="User Image"  style="height: 150px;"/>
            @else
            <!-- Foto Padrao -->
                <img src="/fotos_users/padrao.jpg" alt="logo explorers" class="img-circle" style="height: 150px;">
            @endif
            {!! Form::file('image',['class' => 'text-']) !!}
    </div>      
</div>

<!-- Descricao Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('descricao', 'Descrição (opcional):') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control']) !!}
</div>
@if(Auth::user()->level_acl == 2)

    <hr>        
        <h3 class="text-center">Área Admin</h3>
    <hr>

    <!-- Reputacao Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('reputacao', 'Reputação:') !!}
        {!! Form::number('reputacao', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Code Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('code', 'Código:') !!}
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
    </div>

     <!-- Code Field -->
     <div class="form-group col-sm-6">
        {!! Form::label('admin', 'ADMINISTRADOR:') !!}
        {!! Form::checkbox('admin', null, $admin) !!}
    </div>

    <div class="form-group col-sm-12">
        {!! Form::label('', 'Ofertas:') !!}
    </div>
    @foreach (array_combine($ofertas_list, $checked) as $ofertas => $checked)

        <div class="form-group col-sm-12"> 
            {!! Form::checkbox('ids[]', $ofertas, $checked, ['class' => 'icheckbox_flat-blue']) !!}
            {!! Form::label('nome', App\Oferta::where('id', $ofertas)->first()->nome) !!}
        </div>

    @endforeach
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default">Cancelar</a>
</div>
