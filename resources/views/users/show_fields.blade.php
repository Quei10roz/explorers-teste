<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $users->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $users->email !!}</p>
</div>

<!-- Facebook Field -->
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:') !!}
    <p>{!! $users->facebook !!}</p>
</div>

<!-- Instagram Field -->
<div class="form-group">
    {!! Form::label('instagram', 'Instagram:') !!}
    <p>{!! $users->instagram !!}</p>
</div>

<!-- Twitter Field -->
<div class="form-group">
    {!! Form::label('twitter', 'Twitter:') !!}
    <p>{!! $users->twitter !!}</p>
</div>

<!-- Linkedin Field -->
<div class="form-group">
    {!! Form::label('linkedin', 'Linkedin:') !!}
    <p>{!! $users->linkedin !!}</p>
</div>

<!-- Estado Field -->
<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    <p>{!! $users->estado !!}</p>
</div>

<!-- Cidade Field -->
<div class="form-group">
    {!! Form::label('cidade', 'Cidade:') !!}
    <p>{!! $users->cidade !!}</p>
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descricao:') !!}
    <p>{!! $users->descricao !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Membro desde:') !!}
    <p>{!! date('j M Y', strtotime($users->created_at)) !!}</p>
</div>

