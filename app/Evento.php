<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Evento extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ofertas()
    {
        return $this->belongsTo('Ofeta');
    }
}
