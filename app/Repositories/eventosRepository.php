<?php

namespace App\Repositories;

use App\Models\eventos;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class eventosRepository
 * @package App\Repositories
 * @version July 30, 2018, 11:36 pm UTC
 *
 * @method eventos findWithoutFail($id, $columns = ['*'])
 * @method eventos find($id, $columns = ['*'])
 * @method eventos first($columns = ['*'])
*/
class eventosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'estado',
        'cidade',
        'data',
        'link',
        'descricao',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return eventos::class;
    }
}
