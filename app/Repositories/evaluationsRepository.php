<?php

namespace App\Repositories;

use App\Models\evaluations;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class evaluationsRepository
 * @package App\Repositories
 * @version July 31, 2018, 9:40 pm UTC
 *
 * @method evaluations findWithoutFail($id, $columns = ['*'])
 * @method evaluations find($id, $columns = ['*'])
 * @method evaluations first($columns = ['*'])
*/
class evaluationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ip',
        'nota',
        'nome',
        'conteudo',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return evaluations::class;
    }
}
