<?php

namespace App\Repositories;

use App\Models\comentarios;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class comentariosRepository
 * @package App\Repositories
 * @version August 2, 2018, 4:58 pm UTC
 *
 * @method comentarios findWithoutFail($id, $columns = ['*'])
 * @method comentarios find($id, $columns = ['*'])
 * @method comentarios first($columns = ['*'])
*/
class comentariosRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'autor',
        'conteudo',
        'post_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return comentarios::class;
    }
}
