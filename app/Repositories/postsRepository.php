<?php

namespace App\Repositories;

use App\Models\posts;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class postsRepository
 * @package App\Repositories
 * @version August 2, 2018, 4:57 pm UTC
 *
 * @method posts findWithoutFail($id, $columns = ['*'])
 * @method posts find($id, $columns = ['*'])
 * @method posts first($columns = ['*'])
*/
class postsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'conteudo',
        'image',
        'link-video',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return posts::class;
    }
}
