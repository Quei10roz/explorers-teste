<?php

namespace App\Repositories;

use App\Models\tags;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class tagsRepository
 * @package App\Repositories
 * @version July 31, 2018, 9:02 pm UTC
 *
 * @method tags findWithoutFail($id, $columns = ['*'])
 * @method tags find($id, $columns = ['*'])
 * @method tags first($columns = ['*'])
*/
class tagsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return tags::class;
    }
}
