<?php

namespace App\Repositories;

use App\Models\ofertas;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ofertasRepository
 * @package App\Repositories
 * @version July 30, 2018, 10:09 pm UTC
 *
 * @method ofertas findWithoutFail($id, $columns = ['*'])
 * @method ofertas find($id, $columns = ['*'])
 * @method ofertas first($columns = ['*'])
*/
class ofertasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome',
        'image',
        'descricao'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ofertas::class;
    }
}
