<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
    public function eventos()
    {
        return $this->hasMany('Eventos');
    }
}
