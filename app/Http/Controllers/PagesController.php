<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oferta;
use App\User;
use App\Evento;
use App\Ofertas_Membro;

class PagesController extends Controller
{
    public function getIndex(){
        $eventos = Evento::where('deleted_at', null)->get();
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('index')->with('nav_ofertas', $nav_ofertas)->withEventos($eventos);
    }

    public function getMembros(){
        $users = User::where('deleted_at', null)->orderBy('reputacao','desc')->get();
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.membros')->with('nav_ofertas', $nav_ofertas)->withUsers($users);
    }

    public function getIdealizador(){
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.idealizador')->with('nav_ofertas', $nav_ofertas);
    }

    public function getPerfil($slug){
        $user = User::where('slug', $slug)->where('deleted_at', null)->first();
        $eventos = Evento::where('user_id',$user->id)->where('deleted_at', null)->get();
        $ofertas = Ofertas_Membro::where('user_id',$user->id)->where('deleted_at', null)->get();
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.membros.perfil.usuário')->with('nav_ofertas', $nav_ofertas)->withUser($user)->withEventos($eventos)->withOfertas($ofertas);
    }

    public function getAvalie(){
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.membros.perfil.avalie')->with('nav_ofertas', $nav_ofertas);
    }

    public function getOfertas($slug){
        $oferta = Oferta::where('slug',$slug)->first();
        $users = array();
        $ofertas_membro = Ofertas_Membro::where('ofertas_id', $oferta->id)->get();
        $id_list = array();
        $num = 0;

        foreach($ofertas_membro as $ofertas){
            $users[$num] = User::where('id', $ofertas->user_id)->where('deleted_at', null)->first();
            $num++;
        }

        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.ofertas')->with('nav_ofertas', $nav_ofertas)->withOferta($oferta)->withUsers($users);
    }

    public function getQuemSomos(){
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.quem-somos')->with('nav_ofertas', $nav_ofertas);
    }

    public function getOrigem(){
        return view('pages.origem');
    }

    public function getProduto(){
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.produto')->with('nav_ofertas', $nav_ofertas);
    }

    public function getLivro(){
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.livro')->with('nav_ofertas', $nav_ofertas);
    }

    public function getEventos($slug){
        $evento = Evento::where('slug', $slug)->first();
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.eventos')->with('nav_ofertas', $nav_ofertas)->withEvento($evento);
    }

    public function getDiario(){
        $nav_ofertas = Oferta::where('deleted_at', null)->get();
        return view('pages.diario')->with('nav_ofertas', $nav_ofertas);
    }
}
