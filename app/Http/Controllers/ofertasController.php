<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateofertasRequest;
use App\Http\Requests\UpdateofertasRequest;
use App\Repositories\ofertasRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Input;
use App\Ofertas_Membro;
use App\Oferta;
use App\User;
use Illuminate\Support\Facades\Auth;

class ofertasController extends AppBaseController
{
    /** @var  ofertasRepository */
    private $ofertasRepository;

    public function __construct(ofertasRepository $ofertasRepo)
    {
        $this->ofertasRepository = $ofertasRepo;
    }

    /**
     * Display a listing of the ofertas.
     *
     * @param Request $request
     * @return Response
     */
    
    public function getOfertaSugestoes(Request $request)
    {
        $ofertas = Oferta::where('aprovado', '0')->where('deleted_at', null)->paginate(5);

        return view('ofertas.sugestao.index')
            ->with('ofertas', $ofertas);
    }

    public function getAreaMembro(Request $request)
    {
        $ofertas = Oferta::where('user_id', Auth::id())->where('deleted_at', null)->orderBy('aprovado', 'desc')->paginate(5);

        return view('ofertas.cadastro.index')
            ->with('ofertas', $ofertas);
    }

    public function index(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $ofertas = Oferta::where('aprovado', '1')->where('deleted_at', null)->paginate(5);

        return view('ofertas.index')
            ->with('ofertas', $ofertas);
    }
  

    /**
     * Show the form for creating a new ofertas.
     *
     * @return Response
     */
    public function create()
    {
        return view('ofertas.create');
    }

    /**
     * Store a newly created ofertas in storage.
     *
     * @param CreateofertasRequest $request
     *
     * @return Response
     */
    public function store(CreateofertasRequest $request)
    {
        //VALIDANDO DADOS DO MENININHO
        $request->validate([
            'nome' => 'required|string|max:255|unique:ofertas',
            'image_perfil' => 'required',
            'image_banner' => 'required',
            'descricao' => 'required',
        ]);

        //Criando a slug
        $request->merge(['slug' => AppBaseController::slugify($request->nome)]);

        $request->merge(['user_id' => Auth::id()]);

        $input = $request->all();

        $ofertas = $this->ofertasRepository->create($input);

        //Cria a imagem com o nome bonitinho
        if ($request->has('image_perfil')) $ofertas->image_perfil = $ofertas->id . '.' . 'oferta-p' . '.' . $request->files->all()['image_perfil']->getClientOriginalExtension();
        if ($request->has('image_banner')) $ofertas->image_banner = $ofertas->id . '.' . 'oferta-b' . '.' . $request->files->all()['image_banner']->getClientOriginalExtension();
        
        //Salva com o nome bonitinho
        $ofertas->save();
        //Move a imagem pra pagina public
        if ($request->has('image_perfil'))
        { 
            $fileImage_perfil = Input::file('image_perfil');
            $destinationPath = '../public/fotos_ofertas';
            $fileImage_perfil->move($destinationPath, $ofertas->image_perfil);
            $ofertas->image_perfil = 'fotos_ofertas/' . $request->file('image_perfil')->getClientOriginalName();
        }
        if ($request->has('image_banner'))
        { 
            $fileImage_banner = Input::file('image_banner');
            $destinationPath = '../public/fotos_ofertas';
            $fileImage_banner->move($destinationPath, $ofertas->image_banner);
            $ofertas->image_banner = 'fotos_ofertas/' . $request->file('image_banner')->getClientOriginalName();
        }

        Flash::success('Ofertas saved successfully.');

        return redirect(route('ofertas.cadastro'));
    }

    /**
     * Display the specified ofertas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ofertas = $this->ofertasRepository->findWithoutFail($id);

        if (empty($ofertas)) {
            Flash::error('Ofertas not found');

            return redirect(route('ofertas.index'));
        }

        return view('ofertas.show')->with('ofertas', $ofertas);
    }

    /**
     * Show the form for editing the specified ofertas.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ofertas = $this->ofertasRepository->findWithoutFail($id);

        if (empty($ofertas)) {
            Flash::error('Ofertas not found');

            return redirect(route('ofertas.index'));
        }

        return view('ofertas.edit')->with('ofertas', $ofertas);
    }

    /**
     * Update the specified ofertas in storage.
     *
     * @param  int              $id
     * @param UpdateofertasRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateofertasRequest $request)
    {
        //VALIDANDO DADOS DE ATUALIZACAO
        $request->validate([
            'nome' => 'required|string|max:255',
            'image_perfil' => 'required',
            'image_banner' => 'required',
            'descricao' => 'required',
        ]);

        $ofertas = $this->ofertasRepository->findWithoutFail($id);

        if (empty($ofertas)) {
            Flash::error('Ofertas not found');

            return redirect(route('ofertas.index'));
        }

        //CHECANDO SE O NOME EH UNICO CASO TENHA MUDADO
        if($request->nome != $ofertas->nome){
            $request->validate([
                'nome' => 'unique:ofertas',
            ]);
        }

        //Criando a slug
        $request->merge(['slug' => AppBaseController::slugify($request->nome)]);
        
        $ofertas = $this->ofertasRepository->update($request->all(), $id);


       //Cria a imagem com o nome bonitinho
       if ($request->has('image_perfil')) $ofertas->image_perfil = $ofertas->id . '.' . 'oferta-p' . '.' . $request->files->all()['image_perfil']->getClientOriginalExtension();
       if ($request->has('image_banner')) $ofertas->image_banner = $ofertas->id . '.' . 'oferta-b' . '.' . $request->files->all()['image_banner']->getClientOriginalExtension();
       
       //Salva com o nome bonitinho
       $ofertas->save();
       //Move a imagem pra pagina public
       if ($request->has('image_perfil'))
       { 
           $fileImage_perfil = Input::file('image_perfil');
           $destinationPath = '../public/fotos_ofertas';
           $fileImage_perfil->move($destinationPath, $ofertas->image_perfil);
           $ofertas->image_perfil = 'fotos_ofertas/' . $request->file('image_perfil')->getClientOriginalName();
       }
       if ($request->has('image_banner'))
       { 
           $fileImage_banner = Input::file('image_banner');
           $destinationPath = '../public/fotos_ofertas';
           $fileImage_banner->move($destinationPath, $ofertas->image_banner);
           $ofertas->image_banner = 'fotos_ofertas/' . $request->file('image_banner')->getClientOriginalName();
       }

        Flash::success('Ofertas updated successfully.');

        return redirect(route('ofertas.cadastro'));
    }

    /**
     * Remove the specified ofertas from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $ofertas = $this->ofertasRepository->findWithoutFail($id);

        if (empty($ofertas)) {
            Flash::error('Ofertas not found');

            return redirect(route('ofertas.index'));
        }

        $this->ofertasRepository->delete($id);

        Flash::success('Ofertas deleted successfully.');

        return redirect(route('ofertas.index'));
    }

    public function getMembros($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        //Pegando a oferta para poder passar o nome para a view
        $oferta = $this->ofertasRepository->findWithoutFail($id);
        //Pegando as instancias da tabela pivo
        $ofertas = Ofertas_Membro::where('ofertas_id', $id)->get();
        //Criando variaveis para o armazenamento dos membros
        $num = 0;
        //Criando variavel membros para caso nao haja ofertas
        $membros =  array();
        //Foreach para garantir que todos os membros sejam corretamente adicionados
        foreach($ofertas as $ofertas)
        {
            $membros[$num] = User::where('id', $ofertas->user_id)->where('deleted_at', null)->first();
            $num = $num + 1;
        }   
        
        //Retornando membros para a lista e oferta para o nome
        return view('ofertas.membros')->withMembros($membros)->withOferta($oferta);
    }

    public function getMembrosBusca($id, Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }
        //Pegando a oferta para poder passar o nome para a view
        $oferta = $this->ofertasRepository->findWithoutFail($id);
        //Pegando as instancias da tabela pivo
        $ofertas = Ofertas_Membro::where('ofertas_id', $id)->get();
        //Criando variaveis para o armazenamento dos membros
        $num = 0;
        //Criando variavel membros para caso nao haja ofertas
        $membros =  array();
        //Foreach para garantir que todos os membros sejam corretamente adicionados
        foreach($ofertas as $ofertas)
        {
            $membros[$num] = User::where('id',$ofertas->user_id)->where('name',$request->pesquisa)->where('deleted_at', null)->first();
            $num = $num + 1;
        }   
        //Retornando membros para a lista e oferta para o nome
        return view('ofertas.membros')->withMembros($membros)->withOferta($oferta);
    }

    public function getAdd_membros($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        //Pegando a oferta para poder passar o nome para a view
        $oferta = $this->ofertasRepository->findWithoutFail($id);
        //Pegando a lista de membros
        $membros = User::orderBy('name')->get();
        //Criando pra nao dar problema
        $num = 0;
        $membros_list = array();
        $checked = array();
        //Criando lista com itens checados
        foreach ($membros as $membros){     
            if($membros->deleted_at == null){      
                $check = Ofertas_Membro::where('user_id', $membros->id)->where('ofertas_id', $id)->first();
                $membros_list[$num] = $membros->id;
                if(empty($check)){
                    $checked[$num] = false;                
                }else{
                    $checked[$num] = true;
                }
                $num++;
            }
        }
        return view('ofertas.add_membros')->with('membros_list', $membros_list)->withOferta($oferta)->withChecked($checked);
    }

    public function getAdd_membrosBusca($id, Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }
        //Pegando a oferta para poder passar o nome para a view
        $oferta = $this->ofertasRepository->findWithoutFail($id);
        //Pegando a lista de membros
        $membros = User::orderBy('name')->get();
        //Criando pra nao dar problema
        $num = 0;
        $membros_list = array();
        $checked = array();
        //Criando lista com itens checados
        foreach ($membros as $membros){           
            if($membros->name == $request->pesquisa AND $membros->deleted_at == null){
                $check = Ofertas_Membro::where('user_id', $membros->id)->where('ofertas_id', $id)->where('deleted_at', null)->first();
                $membros_list[$num] = $membros->id;
                if(empty($check)){
                    $checked[$num] = false;                
                }else{
                    $checked[$num] = true;
                }
                $num++;
            }            
        }        
        return view('ofertas.add_membros')->with('membros_list', $membros_list)->withOferta($oferta)->withChecked($checked);
    }


    public function postAdd_membros($id, Request $request)
    {
   
        //Recebendo ids de quem foi checado
        $lista_user_id = $request->ids;
        //Apagando lista antiga
        $lista_antiga = Ofertas_Membro::where('ofertas_id', $id)->delete();
        
        if($request->has('ids')){
            foreach ($lista_user_id as $user_id){
    
                    $ofertas_membro = new Ofertas_Membro;
                    
                    $ofertas_membro->user_id = $user_id;
                    
                    $ofertas_membro->ofertas_id = $id;
                    
                    $ofertas_membro->save();     
            }
        }
        //-------------------------------------MESMO CODIGO DE GETMEMBRO
        //Pegando a oferta para poder passar o nome para a view
        $oferta = $this->ofertasRepository->findWithoutFail($id);
        //Pegando as instancias da tabela pivo
        $ofertas = Ofertas_Membro::where('ofertas_id', $id)->get();
        //Criando variavel membros para caso nao haja ofertas
        $membros =  array();
        //Criando variaveis para o armazenamento dos membros
        $num = 0;
        //Foreach para garantir que todos os membros sejam corretamente adicionados
        foreach($ofertas as $ofertas)
        {
            $membros[$num] = User::find($ofertas->user_id);
            $num = $num + 1;
        }   
        //Retornando membros para a lista e oferta para o nome
        return view('ofertas.membros')->withMembros($membros)->withOferta($oferta);
    }

    public function getAprovarOferta(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $oferta = Oferta::where('id', $request->id)->where('deleted_at', null)->first();
        $oferta->aprovado = '1';
        $oferta->save();

        $ofertas = Oferta::where('aprovado', '1')->where('deleted_at', null)->paginate(5);

        return view('ofertas.index')
            ->with('ofertas', $ofertas);
    }

    public function getNegarOferta(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $oferta = Oferta::where('id', $request->id)->where('deleted_at', null)->first();
        $oferta->aprovado = '-1';
        $oferta->save();

        $ofertas = Oferta::where('aprovado', '1')->where('deleted_at', null)->paginate(5);

        return view('ofertas.index')
            ->with('ofertas', $ofertas);
    }
}
