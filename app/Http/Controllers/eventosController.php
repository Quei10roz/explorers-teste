<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateeventosRequest;
use App\Http\Requests\UpdateeventosRequest;
use App\Repositories\eventosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Oferta;
use App\Evento;
use App\Ofertas_Membro;
use Illuminate\Support\Facades\Auth;

class eventosController extends AppBaseController
{
    /** @var  eventosRepository */
    private $eventosRepository;

    public function __construct(eventosRepository $eventosRepo)
    {
        $this->eventosRepository = $eventosRepo;
    }

    /**
     * Display a listing of the eventos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }  
        
        $eventos = Evento::where('deleted_at', null)->orderBy('created_at', 'desc')->paginate(5);       
        return view('eventos.index')
            ->with('eventos', $eventos);
    }

    public function indexSeusEventos(Request $request)
    {
        $eventos = Evento::where('user_id',Auth::id())->where('deleted_at', null)->orderBy('created_at', 'desc')->paginate(5);
      
        return view('eventos.index')
            ->with('eventos', $eventos);
    }

    /**
     * Show the form for creating a new eventos.
     *
     * @return Response
     */
    public function create()
    {
        $ofertas = Oferta::where('deleted_at', null)->get();
        $ofertas_list = array();
        foreach ($ofertas as $oferta){
            $ofertas_list += [$oferta->nome => $oferta->nome];
        }
        return view('eventos.create')->withOfertas($ofertas)->with('ofertas_list', $ofertas_list);
    }

    /**
     * Store a newly created eventos in storage.
     *
     * @param CreateeventosRequest $request
     *
     * @return Response
     */
    public function store(CreateeventosRequest $request)
    {
        
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'titulo' => 'required|string|max:255|unique:eventos',
            'cidade' => 'required|string|max:255',
            'link' => 'required|string|max:255',
            'data' => 'required|date',
        ]);

        $request->merge(['user_id' => Auth::id()]);

        /*

        assim (pegando tudo)

        $request->oferta_id = Oferta::where('nome',$request->oferta_id)->first();
        $input = $request->all();

        ou assim (pegando so o id)

        $oferta_id = Oferta::where('nome',$request->oferta_id)->first();
        $request->oferta_id = $oferta_id->id;

        //*/
        
        //Checando se a ofeta existe
        $oferta = Oferta::where('nome',$request->oferta)->first();
        
        if (empty($oferta)) {
            Flash::error('Oferta Inválida.');

            return redirect(route('seuseventos'));
        }
        
        //Checado se o rapaz tem altorizacao para participar daquela oferta
        $oferta_membro_check = Ofertas_Membro::where('ofertas_id',$oferta->id)->where('user_id',Auth::id())->first();

        if (empty($oferta_membro_check)) {

            Flash::error('Você não está autorizado a criar eventos dessa oferta.');

            return redirect(route('seuseventos'));
        }

        //Adicionando o campo de ofeta_id para a criacao da tabela
        $request->merge(['oferta_id' => $oferta->id]);

        //Criando a slug
        $request->merge(['slug' => AppBaseController::slugify($request->titulo)]);

        $input = $request->all();
        
        $eventos = $this->eventosRepository->create($input);

        Flash::success('Evento salvo com sucesso.');

        return redirect(route('seuseventos'));
    }

    /**
     * Display the specified eventos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $eventos = $this->eventosRepository->findWithoutFail($id);

        if (empty($eventos)) {
            Flash::error('Evento não encontrado.');

            return redirect(route('seuseventos'));
        }

        //checando se o cara eh admin pra ver pra onde ele vai
        if($eventos->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        return view('eventos.show')->with('eventos', $eventos);
    }

    /**
     * Show the form for editing the specified eventos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $eventos = $this->eventosRepository->findWithoutFail($id);

        if (empty($eventos)) {
            Flash::error('Evento não encontrado.');

            return redirect(route('seuseventos'));
        }

        //checando se o cara eh admin pra ver pra onde ele vai
        if($eventos->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        //Checando se fulaninho pode editar tal coisa, pular isso caso ele seja admin
        if($eventos->user_id != Auth::id()){
            Flash::error('Você não está autorizado a alterar esse evento.');

            return redirect(route('seuseventos'));
        }

        //Ofertas para select
        $ofertas = Oferta::where('deleted_at', null)->get();
        $ofertas_list = array();
        foreach ($ofertas as $oferta){
            $ofertas_list += [$oferta->nome => $oferta->nome];
        }

        return view('eventos.edit')->with('eventos', $eventos)->with('ofertas_list', $ofertas_list);
    }

    /**
     * Update the specified eventos in storage.
     *
     * @param  int              $id
     * @param UpdateeventosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateeventosRequest $request)
    {       
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'titulo' => 'required|string|max:255',
            'cidade' => 'required|string|max:255',
            'link' => 'required|string|max:255',
            'data' => 'required|date',
        ]);

        $eventos = $this->eventosRepository->findWithoutFail($id);    
        
        //checando se o cara eh admin pra ver pra onde ele vai
        if($eventos->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        if (empty($eventos)) {
            Flash::error('Evento não encontrado.');

            return redirect(route('seuseventos'));
        }

        if($request->titulo != $eventos->titulo){
            //VALIDAÇÃO DOS DADOS RECEBIDOS
            $request->validate([
                'titulo' => 'unique:eventos',
            ]);
        }

        //Checando se a ofeta existe
        $oferta = Oferta::where('nome',$request->oferta)->first();
        if (empty($oferta)) {
            Flash::error('Oferta Inválida.');

            return redirect(route('seuseventos'));
        }

        //Checado se o rapaz tem altorizacao para participar daquela oferta
        $oferta_membro_check = Ofertas_Membro::where('ofertas_id',$oferta->id)->where('user_id',Auth::id())->first();
        if (empty($oferta_membro_check)) {

            Flash::error('Você não está autorizado a criar eventos dessa oferta.');

            return redirect(route('seuseventos'));
        }

        //Adicionando o campo de ofeta_id para a criacao da tabela
        $request->merge(['oferta_id' => $oferta->id]);

        //Criando e adicionando slug a request
        $request->merge(['slug' => AppBaseController::slugify($request->titulo)]);

        $eventos = $this->eventosRepository->update($request->all(), $id);

        Flash::success('Evento salvo com sucesso.');

        return redirect(route('seuseventos'));
    }

    /**
     * Remove the specified eventos from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //Checando se fulaninho pode editar tal coisa, pular isso caso ele seja admin
        if($id != Auth::id() AND Auth::user()->level_acl == 1){
            return redirect('/home');
        }
        
        $eventos = $this->eventosRepository->findWithoutFail($id);

        if (empty($eventos)) {
            Flash::error('Evento não encontrado.');

            return redirect(route('seuseventos'));
        }

        $this->eventosRepository->delete($id);

        Flash::success('Evento deletado com sucesso.');

        return redirect(route('seuseventos'));
    }
}
