<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatepostsRequest;
use App\Http\Requests\UpdatepostsRequest;
use App\Repositories\postsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Flash;
use App\Tag;
use App\Post;
use App\Tags_Post;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Input;
use Response;

class postsController extends AppBaseController
{
    /** @var  postsRepository */
    private $postsRepository;

    public function __construct(postsRepository $postsRepo)
    {
        $this->postsRepository = $postsRepo;
    }

    /**
     * Display a listing of the posts.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        $posts = Post::where('deleted_at', null)->orderBy('created_at', 'desc')->paginate(5);

        return view('posts.index')
            ->with('posts', $posts);
    }

    public function indexSeusPosts(Request $request)
    {
        $posts = Post::where('user_id',Auth::id())->where('deleted_at', null)->orderBy('created_at', 'desc')->paginate(5);
        return view('posts.index')
            ->with('posts', $posts);
    }

    public function indexBusca(Request $request)
    {
        $posts = Post::where('titulo',$request->pesquisa)->where('deleted_at', null)->paginate();

        //checando se o cara eh admin pra ver pra onde ele vai
        if($posts->user_id != Auth::user() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        return view('posts.index')
            ->with('posts', $posts);
    }

    public function typeahead_post(Request $request){
        $cont = 0;
        $posts = \DB::table('posts')->select('titulo')->where('titulo', 'like', $request->post.'%')->get();//->where('equipe', 'like', '%'.$request->equipe.'%')->groupby('equipe')->get();
        foreach($posts as $post){
            $temp =  $post->titulo;
            if($temp != null)
            $resultado[++$cont] = $temp;
        }
        $resultado[0] = 1;

        return \Response::json($resultado);
    }

    /**
     * Show the form for creating a new posts.
     *
     * @return Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created posts in storage.
     *
     * @param CreatepostsRequest $request
     *
     * @return Response
     */
    public function store(CreatepostsRequest $request)
    {
        //Adiciona id do usuario para a criacao da tabela
        $request->merge(['user_id' => Auth::id()]);

        //Adicionando campo slug
        $request->merge(['slug' => AppBaseController::slugify($request->titulo)]);

        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'titulo' => 'required|string|max:300|unique:posts',
            'tags' => 'nullable|string|max:255',
            'conteudo' => 'required|string|max:2000',
            'image' => 'nullable|string',
            'link_video' => 'nullable|string',
        ]);
        
        //checa se existe
        if(!empty($request->tags)){
            //pega a string de tags
            $tags = $request->tags;
            //transforma a string em um vetor
            $tags = explode(',', $tags);       
            //checa se cada um dos campos realmente existe
            foreach($tags as $tag){
                $tag_checker = Tag::where('nome', $tag)->first();
                if(empty($tag_checker)){
                    Flash::error('Tag ' . $tag . ' não existe.');

                    return redirect(route('seusposts'));
                }
            }
        }
        
        $input = $request->all();
        
        $posts = $this->postsRepository->create($input);
        
        //pega a string de tags
        $tags = $request->tags;
        //transforma a string em um vetor
        $tags = explode(',', $tags);  
        //Cria as tabelas pivo entre tag e post
        if(!empty($request->tags)){
            foreach($tags as $tag){
            $tags_post = new Tags_Post;
            $tags_post->tag_id = Tag::where('nome', $tag)->first()->id;
            $tags_post->post_id = $posts->id;
            $tags_post->save();
            }
        }
        
        //Cria a imagem com o nome bonitinho
        if ($request->has('image')) $posts->image = $posts->id . '.' . 'post' . '.' . $request->files->all()['image']->getClientOriginalExtension();
         
        //Salva com o nome bonitinho
        $posts->save();
        //Move a imagem pra pagina public
        if ($request->has('image'))
        { 
            $fileImage = Input::file('image');
            $destinationPath = '../public/fotos_posts';
            $fileImage->move($destinationPath, $posts->image);
            $posts->image = 'fotos_posts/' . $request->file('image')->getClientOriginalName();
        }

        Flash::success('Posts saved successfully.');

        return redirect(route('seusposts'));
    }

    /**
     * Display the specified posts.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $posts = $this->postsRepository->findWithoutFail($id);
     
        if (empty($posts)) {
            Flash::error('Posts not found');

            return redirect(route('seusposts'));
        }
           
        //Checando se fulaninho pode editar tal coisa, pular isso caso ele seja admin
        if($posts->user_id != Auth::id() AND Auth::user()->level_acl == 1){
            return redirect('/home');
        }

        return view('posts.show')->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified posts.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $posts = $this->postsRepository->findWithoutFail($id);

        if (empty($posts)) {
            Flash::error('Posts not found');

            return redirect(route('posts.index'));
        }

        //Checando se fulaninho pode editar tal coisa, pular isso caso ele seja admin
        if($posts->user_id != Auth::id() AND Auth::user()->level_acl == 1){
            return redirect('/home');
        }

        return view('posts.edit')->with('posts', $posts);
    }

    /**
     * Update the specified posts in storage.
     *
     * @param  int              $id
     * @param UpdatepostsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepostsRequest $request)
    {
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'titulo' => 'required|string|max:300',
            'tags' => 'nullable|string|max:255',
            'conteudo' => 'required|string|max:2000',
            'image' => 'nullable|string',
            'link_video' => 'nullable|string',
        ]);

        //Adicionando campo slug
        $request->merge(['slug' => AppBaseController::slugify($request->titulo)]);
        
        //checa se existe
        if(!empty($request->tags)){
            //pega a string de tags
            $tags = $request->tags;
            //transforma a string em um vetor
            $tags = explode(',', $tags);       
            //checa se cada um dos campos realmente existe
            foreach($tags as $tag){
                $tag_checker = Tag::where('nome', $tag)->first();
                if(empty($tag_checker)){
                    Flash::error('Tag ' . $tag . ' não existe.');

                    return redirect(route('seusposts'));
                }
            }
        }

        $posts = $this->postsRepository->findWithoutFail($id);

        if (empty($posts)) {
            Flash::error('Posts not found');

            return redirect(route('posts.index'));
        }

        //Checando se fulaninho pode editar tal coisa, pular isso caso ele seja admin
        if($posts->user_id != Auth::id() AND Auth::user()->level_acl == 1){
            return redirect('/home');
        }
        
        if($posts->titulo != $request->titulo){
            //VALIDAÇÃO DOS DADOS RECEBIDOS
            $request->validate([
                'titulo' => 'unique:posts',
            ]);
        }

        $posts = $this->postsRepository->update($request->all(), $id);

        //pega a string de tags
        $tags = $request->tags;
        //transforma a string em um vetor
        $tags = explode(',', $tags);  
        //Cria as tabelas pivo entre tag e post
        if(!empty($request->tags)){
            foreach($tags as $tag){
            $tags_post = new Tags_Post;
            $tags_post->tag_id = Tag::where('nome', $tag)->first()->id;
            $tags_post->post_id = $posts->id;
            $tags_post->save();
            }
        }
        
        //Cria a imagem com o nome bonitinho
        if ($request->has('image')) $posts->image = $posts->id . '.' . 'post' . '.' . $request->files->all()['image']->getClientOriginalExtension();
         
        //Salva com o nome bonitinho
        $posts->save();
        //Move a imagem pra pagina public
        if ($request->has('image'))
        { 
            $fileImage = Input::file('image');
            $destinationPath = '../public/fotos_posts';
            $fileImage->move($destinationPath, $posts->image);
            $posts->image = 'fotos_posts/' . $request->file('image')->getClientOriginalName();
        }

        Flash::success('Posts updated successfully.');

        return redirect(route('seusposts'));
    }

    /**
     * Remove the specified posts from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //Checando se fulaninho pode editar tal coisa, pular isso caso ele seja admin
        if($id != Auth::id() AND Auth::user()->level_acl == 1){
            return redirect('/home');
        }

        $posts = $this->postsRepository->findWithoutFail($id);

        if (empty($posts)) {
            Flash::error('Posts not found');

            return redirect(route('seusposts'));
        }

        $this->postsRepository->delete($id);

        Flash::success('Posts deleted successfully.');

        return redirect(route('seusposts'));
    }
}
