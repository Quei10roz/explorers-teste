<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function slugify($string) {
        $string = preg_replace('/[\t\n]/', ' ', $string);
        $string = preg_replace('/\s{2,}/', ' ', $string);
        $list = array(
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
            '/' => '-',
            ' ' => '-',
            '.' => '-',
        );
    
        $string = strtr($string, $list);
        $string = preg_replace('/-{2,}/', '-', $string);
        $string = strtolower($string);
    
        return $string;
    }
    
    public static function decrip($string){            
        $string = $string . '==';
        
        $string = base64_decode($string);
        
        $strings = explode('-', $string);
        $sup = array();
        $alfa = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $alfa_dict = [  'A' => 0,
                        'B' => 1,
                        'C' => 2,
                        'D' => 3,
                        'E' => 4,
                        'F' => 5,
                        'G' => 6,
                        'H' => 7,
                        'I' => 8,
                        'J' => 9,
                        'K' => 10,
                        'L' => 11,
                        'M' => 12,
                        'N' => 13,
                        'O' => 14,
                        'P' => 15,
                        'Q' => 16,
                        'R' => 17,
                        'S' => 18,
                        'T' => 19,
                        'U' => 20,
                        'V' => 21,
                        'W' => 22,
                        'X' => 23,
                        'Y' => 24,
                        'Z' => 25 ];
        $num = 0;
        $valor_final = 0;

        foreach ($strings as $parts){
            $parts = str_split($parts);
            foreach ($parts as $char){
                if (in_array($char, $alfa)) { 
                    array_push($sup, $char);
                }         
            }
        }
        
        foreach ($sup as $char){
            $valor = 25 ** $num;
            $valor_final = $valor_final + ($valor * $alfa_dict[$char]);
            $valor = 0;
            $num++;
        }

        $valor_final = $valor_final - 40;

        if($valor_final % 367867 == 0){
            return true;
        }else{
            return false;
        }
    }
}
