<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateevaluationsRequest;
use App\Http\Requests\UpdateevaluationsRequest;
use App\Repositories\evaluationsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use App\evaluation;
use App\Evento;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth;

class evaluationsController extends AppBaseController
{
    /** @var  evaluationsRepository */
    private $evaluationsRepository;

    public function __construct(evaluationsRepository $evaluationsRepo)
    {
        $this->evaluationsRepository = $evaluationsRepo;
    }

    /**
     * Display a listing of the evaluations.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }  

        $evaluations = evaluation::where('deleted_at', null)->orderBy('user_id','desc')->paginate(5);
        return view('evaluations.index')
            ->with('evaluations', $evaluations);
    }
    
    public function indexSuasAvaliacoes(Request $request)
    {
        $this->evaluationsRepository->pushCriteria(new RequestCriteria($request));
        $evaluations = $this->evaluationsRepository->all();
        $evaluations = evaluation::where('user_id',Auth::id())->where('deleted_at', null)->orderBy('favorito','desc')->paginate(5);
        return view('evaluations.index')
            ->with('evaluations', $evaluations);
    }

    public function indexAvaliacoesEvento($id)
    {
        
        $evaluations = evaluation::where('evento_id', $id)->where('deleted_at', null)->orderBy('user_id','desc')->paginate(5);

        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }  

        return view('evaluations.index')
            ->with('evaluations', $evaluations);
    }

    /**
     * Show the form for creating a new evaluations.
     *
     * @return Response
     */
    public function create()
    {   
        return view('evaluations.create');
    }

    /**
     * Store a newly created evaluations in storage.
     *
     * @param CreateevaluationsRequest $request
     *
     * @return Response
     */
    public function store(CreateevaluationsRequest $request)
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        
        $request->merge(['ip' => $ipaddress]);

        $request->merge(['user_id' => Evento::find($request->evento_id)->user_id]);

        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $avali = evaluation::where('evento_id', $request->evento_id)->where('ip', $ipaddress)->where('deleted_at', null)->first();
        if(!empty($avali)){
            Flash::error('Voce ja opinou sobre esse evento.');

            return redirect(route('evaluations.index'));
        }
        $request->validate([
            'ip' => 'required|string|max:255',
            'nota' => 'required|integer|min:1|max:5',
            'nome' => 'nullable|string|max:255',
            'conteudo' => 'nullable|string|max:1000',
        ]);

        $input = $request->all();

        $evaluations = $this->evaluationsRepository->create($input);

        Flash::success('Evaluations saved successfully.');

        return redirect(route('evaluations.index'));
    }

    /**
     * Display the specified evaluations.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $evaluations = $this->evaluationsRepository->findWithoutFail($id);

        if (empty($evaluations)) {
            Flash::error('Avaliação não encontrada.');

            return redirect(route('suasavaliacoes'));
        }

         //checando se o cara eh admin pra ver pra onde ele vai
         if($evaluations->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        return view('evaluations.show')->with('evaluations', $evaluations);
    }

    /**
     * Show the form for editing the specified evaluations.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //if(Auth::user()->level_acl ==  1){
        
        //}

        $evaluations = $this->evaluationsRepository->findWithoutFail($id);

        if (empty($evaluations)) {
            Flash::error('Avaliação não encontrada.');

            return redirect(route('suasavaliacoes'));
        }

        return view('evaluations.edit')->with('evaluations', $evaluations);
    }
    /**
     * Update the specified evaluations in storage.
     *
     * @param  int              $id
     * @param UpdateevaluationsRequest $request
     *
     * @return Response
     
    */
    public function update($id, UpdateevaluationsRequest $request)
    {
        //if(Auth::user()->level_acl ==  1){
        
        //}
        $evaluations = $this->evaluationsRepository->findWithoutFail($id);

        if (empty($evaluations)) {
            Flash::error('Avaliação não encontrada.');

            return redirect(route('suasavaliacoes'));
        }

        $evaluations = $this->evaluationsRepository->update($request->all(), $id);

        Flash::success('Evaluations updated successfully.');

        return redirect(route('suasavaliacoes'));
    }

    /**
     * Remove the specified evaluations from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $evaluations = $this->evaluationsRepository->findWithoutFail($id);

        if (empty($evaluations)) {
            Flash::error('Avaliação não encontrada.');

            return redirect(route('suasavaliacoes'));
        }

        //checando se o cara eh admin pra ver pra onde ele vai
        if( $evaluations->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        $this->evaluationsRepository->delete($id);

        Flash::success('Evaluations deleted successfully.');

        return redirect(route('suasavaliacoes'));
    }

    public function getFavorito($id){
        $avaliacao = evaluation::find($id);

        //checando se o cara eh admin pra ver pra onde ele vai
        if($avaliacao->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        $avaliacao->favorito? $avaliacao->favorito = false : $avaliacao->favorito = true ;
        $avaliacao->save();

        if(Auth::user()->level_acl ==  1){
            return redirect(route('suasavaliacoes'));
        }
        return redirect(route('evaluations.index'));
    }
}
