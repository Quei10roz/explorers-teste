<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatecomentariosRequest;
use App\Http\Requests\UpdatecomentariosRequest;
use App\Repositories\comentariosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use App\Comentario;
use App\Post;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\Auth;
use Response;
use Illuminate\Support\Facades\DB;

class comentariosController extends AppBaseController
{
    /** @var  comentariosRepository */
    private $comentariosRepository;

    public function __construct(comentariosRepository $comentariosRepo)
    {
        $this->comentariosRepository = $comentariosRepo;
    }

    /**
     * Display a listing of the comentarios.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $comentarios = Comentario::where('deleted_at',null)->orderBy('created_at','desc')->paginate(5);

        return view('comentarios.index')
            ->with('comentarios', $comentarios);
    }

    public function indexSeusComentarios(Request $request)
    {
        $comentarios = DB::table('comentarios')->join('posts', 'posts.id', '=', 'comentarios.post_id')->select('comentarios.*')->where('posts.user_id',Auth::id())->where('comentarios.deleted_at', null)->orderBy('id', 'desc')->paginate(5);
        //dd($comentarios);
        return view('comentarios.index')
            ->with('comentarios', $comentarios);
    }

    public function comentariosPost($id){  
        $posts = Post::find($id);
        //checando se o cara eh admin pra ver pra onde ele vai
        if($posts->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $comentarios = DB::table('comentarios')->join('posts', 'posts.id', '=', 'comentarios.post_id')->select('comentarios.*')->where('posts.id', $id)->where('comentarios.deleted_at', null)->orderBy('id', 'desc')->paginate(5);
        //dd($comentarios);
        return view('comentarios.index')
            ->with('comentarios', $comentarios);
    }

    /**
     * Show the form for creating a new comentarios.
     *
     * @return Response
     */
    public function create()
    {
        return view('comentarios.create');
    }

    /**
     * Store a newly created comentarios in storage.
     *
     * @param CreatecomentariosRequest $request
     *
     * @return Response
     */
    public function store(CreatecomentariosRequest $request)
    {
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'autor' => 'nullable|string|max:255',
            'conteudo' => 'required|string|max:500',
        ]);

        $input = $request->all();

        $comentarios = $this->comentariosRepository->create($input);

        Flash::success('Comentarios saved successfully.');

        return redirect(route('seuscomentarios'));
    }

    /**
     * Display the specified comentarios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $comentarios = $this->comentariosRepository->findWithoutFail($id);

        if (empty($comentarios)) {
            Flash::error('Comentarios not found');

            return redirect(route('seuscomentarios'));
        }

        $posts = Post::find($comentarios->post_id);
        //checando se o cara eh admin pra ver pra onde ele vai
        if($posts->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        return view('comentarios.show')->with('comentarios', $comentarios);
    }

    /**
     * Show the form for editing the specified comentarios.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        return redirect('/home');
        $comentarios = $this->comentariosRepository->findWithoutFail($id);

        if (empty($comentarios)) {
            Flash::error('Comentarios not found');

            return redirect(route('seuscomentarios'));
        }

        $posts = Post::find($comentarios->post_id);
        //checando se o cara eh admin pra ver pra onde ele vai
        if($posts->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        return view('comentarios.edit')->with('comentarios', $comentarios);
    }

    /**
     * Update the specified comentarios in storage.
     *
     * @param  int              $id
     * @param UpdatecomentariosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatecomentariosRequest $request)
    {
        return redirect('/home');
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'autor' => 'nullable|string|max:255',
            'conteudo' => 'required|string|max:500',
        ]);

        $comentarios = $this->comentariosRepository->findWithoutFail($id);

        if (empty($comentarios)) {
            Flash::error('Comentarios not found');

            return redirect(route('seuscomentarios'));
        }

        $comentarios = $this->comentariosRepository->update($request->all(), $id);

        Flash::success('Comentarios updated successfully.');

        return redirect(route('seuscomentarios'));
    }

    /**
     * Remove the specified comentarios from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $comentarios = $this->comentariosRepository->findWithoutFail($id);

        if (empty($comentarios)) {
            Flash::error('Comentarios not found');

            return redirect(route('seuscomentarios'));
        }

        $posts = Post::find($comentarios->post_id);
        //checando se o cara eh admin pra ver pra onde ele vai
        if($posts->user_id != Auth::id() AND Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $this->comentariosRepository->delete($id);

        Flash::success('Comentarios deleted successfully.');

        return redirect(route('seuscomentarios'));
    }
}
