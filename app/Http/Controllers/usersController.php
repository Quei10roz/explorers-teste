<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateusersRequest;
use App\Http\Requests\UpdateusersRequest;
use App\Repositories\usersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Oferta;
use App\Ofertas_Membro;
use Illuminate\Support\Facades\Auth;

class usersController extends AppBaseController
{
    /** @var  usersRepository */
    private $usersRepository;

    public function __construct(usersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * Display a listing of the users.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }   

        $this->usersRepository->pushCriteria(new RequestCriteria($request));
        $users = User::where('deleted_at', null)->orderBy('reputacao', 'desc')->paginate(5);

        $userslist = User::where('deleted_at', null)->get();
        return view('users.index')
            ->with('users', $users)->with('userslist', $userslist);
    }

    public function indexBusca(Request $request)
    {
        //dd($request->request);
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }

        $users = User::where('name', 'like', $request->user.'%')->where('deleted_at', null)->paginate(5);

        return view('users.index')
            ->with('users', $users);
    }

    public function typeahead_user(Request $request){
        $cont = 0;
        $users = \DB::table('users')->select('name')->where('name', 'like', $request->user.'%')->get();//->where('equipe', 'like', '%'.$request->equipe.'%')->groupby('equipe')->get();
        foreach($users as $user){
            $temp =  $user->name;
            if($temp != null)
            $resultado[++$cont] = $temp;
        }
        $resultado[0] = 1;

        return \Response::json($resultado);
    }

    /**
     * Show the form for creating a new users.
     *
     * @return Response
     */
    public function create()
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }  
        //Pegando a lista de ofertas
        $ofertas = Oferta::where('deleted_at', null)->orderBy('nome')->get();
        $num = 0;
        //Iniciando antes para nao dar bug caso checked e ofertas nao existam
        $checked = array();
        $ofertas_list = array();        
        //Criando lista com itens checados
        foreach ($ofertas as $ofertas){     
            $ofertas_list[$num] = $ofertas->id;     
            $checked[$num] = false;
            $num++;
        }
        $admin = false;
        $user_flag = 1;
        return view('users.create')->with('ofertas_list',$ofertas_list)->withChecked($checked)->withAdmin($admin)->with('user_flag',$user_flag);
    }

    /**
     * Store a newly created users in storage.
     *
     * @param CreateusersRequest $request
     *
     * @return Response
     */
    public function store(CreateusersRequest $request)
    {
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'code' => 'required|string|max:255|unique:users',
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        
        //Verificando codigo valido
        if(!Controller::decrip($request->code)){
            Flash::error('Código inválido');

            return redirect(route('users.index'));
        }

        //Criptografa a senha
        $request->merge(['password' => bcrypt($request->password)]);

        //Criando a slug
        $request->merge(['slug' => usersController::slugify($request->name)]);

        //Cria ficha de usuario
        $input = $request->all();        
        $users = $this->usersRepository->create($input);      

        //Cria a imagem com o nome bonitinho
        if ($request->has('image')) $users->image = $users->id . '.' . 'profile' . '.' . $request->files->all()['image']->getClientOriginalExtension();

        //Salva com o nome bonitinho
        $users->save();
        
        //Move a imagem pra pagina public
        if ($request->has('image'))
        { 
            $fileImage = Input::file('image');
            $destinationPath = '../public/fotos_users';
            $fileImage->move($destinationPath, $users->image);
            //$users->image = 'fotos_users/' . $request->file('image')->getClientOriginalName();
        }

        //vendo se o menino eh adm depois de atualizar
        if($request->admin == 'on'){
            $users->level_acl = 2;
            $users->save();
        }else{
            $users->level_acl = 1;
            $users->save();
        }

        Flash::success('Membro adicionado com sucesso.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }  

        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Membro não encontrado');

            return redirect(route('users.index'));
        }
        
        return view('users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified users.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //Checando se fulaninho pode editar tal coisa, pular isso caso ele seja admin
        if($id != Auth::id() AND Auth::user()->level_acl == 1){
            Flash::error('Você não está autorizado a alterar esse membro.');

            return redirect(route('users.edit', [Auth::id()]));
        }

        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Membro não encontrado.');

            return redirect(route('users.index'));
        }

        //Pegando a lista de ofertas
        $ofertas = Oferta::where('deleted_at', null)->orderBy('nome')->get();
        //Criando ofertaslist para nao bugar caso nao exista
        $ofertas_list = array();
        $checked = array();
        $num = 0;
        //Criando lista com itens checados
        foreach ($ofertas as $ofertas){           
            $check = Ofertas_Membro::where('user_id', $id)->where('ofertas_id', $ofertas->id)->first();
            $ofertas_list[$num] = $ofertas->id;
            if(empty($check)){
                $checked[$num] = false;                
            }else{
                $checked[$num] = true;
            }
            $num++;
        }
        //if de uma linha para checar como a checkbox vai ta
        $teste = User::find($id);
        $teste->level_acl == 1 ? $admin = false : $admin = true;
        $teste->image == null ? $user_flag = 1 : $user_flag = 2;
        return view('users.edit')->with('users', $users)->with('ofertas_list',$ofertas_list)->withChecked($checked)->withAdmin($admin)->with('user_flag', $user_flag);
    }

    /**
     * Update the specified users in storage.
     *
     * @param  int              $id
     * @param UpdateusersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateusersRequest $request)
    {
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'code' => 'string|max:255',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $users = $this->usersRepository->findWithoutFail($id);
        
        if (empty($users)) {
            Flash::error('Membro não encontrado.');

            return redirect(route('users.index'));
        }

        //Vendo se o menininho trocou o nome pra poder validar
        if($users->name != $request->name){
            $request->validate([
                'name' => 'unique:users',
            ]);
        }
        //Vendo se o menininho trocou o email pra poder validar
        if($users->email != $request->email){
            $request->validate([
                'email' => 'unique:users',
            ]);
        }
        //Vendo se o menininho trocou o code nome pra poder validar
        if($users->code != $request->code){
            $request->validate([
                'code' => 'unique:users',
            ]);
        }

        //Atualiza senha
        if ($request->password == null)
            $request->merge(array('password' => \App\User::find($id)->password));
        else
            $request->merge(array('password' => bcrypt($request->password)));
            
        //Criando a slug
        $request->merge(['slug' => usersController::slugify($request->name)]);

        //Atualiza dados
        $users = $this->usersRepository->update($request->all(), $id);
        //Cria a imagem com o nome bonitinho
        if ($request->has('image')) $users->image = $users->id . '.' . 'profile' . '.' . $request->files->all()['image']->getClientOriginalExtension();
        //Salva com o nome bonitinho
        $users->save();
        //Move a imagem pra pagina public
        if ($request->has('image'))
        { 
            $fileImage = Input::file('image');
            $destinationPath = '../public/fotos_users';
            $fileImage->move($destinationPath, $users->image);
            $users->image = 'fotos_users/' . $request->file('image')->getClientOriginalName();
        }        
        //Recebendo ids de quem foi checado
        $lista_ofertas_id = $request->ids;
        //Apagando lista antiga
        $lista_antiga = Ofertas_Membro::where('user_id', $id)->delete();
        
        if($request->has('ids')){
            foreach ($lista_ofertas_id as $ofertas_id){
    
                    $ofertas_membro = new Ofertas_Membro;
                    
                    $ofertas_membro->user_id = $id;
                    
                    $ofertas_membro->ofertas_id = $ofertas_id;
                    
                    $ofertas_membro->save();     
            }
        }
        
        //vendo se o menino eh adm depois de atualizar
        if($request->admin == 'on'){
            $user = User::find($id);
            $user->level_acl = 2;
            $user->save();
        }else{
            $user = User::find($id);
            $user->level_acl = 1;
            $user->save();
        }

        Flash::success('Membro atualizado.');

        return redirect(route('users.edit', [$users->id]))->with('users', $users);
    }

    /**
     * Remove the specified users from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        }  
        
        $users = $this->usersRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Membro não encontrado.');

            return redirect(route('users.index'));
        }

        $this->usersRepository->delete($id);

        Flash::success('Membro desligado com sucesso.');

        return redirect(route('users.index'));
    }
}
