<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatetagsRequest;
use App\Http\Requests\UpdatetagsRequest;
use App\Repositories\tagsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Illuminate\Support\Facades\Auth;

class tagsController extends AppBaseController
{
    /** @var  tagsRepository */
    private $tagsRepository;

    public function __construct(tagsRepository $tagsRepo)
    {
        $this->tagsRepository = $tagsRepo;
    }

    /**
     * Display a listing of the tags.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        $this->tagsRepository->pushCriteria(new RequestCriteria($request));
        $tags = $this->tagsRepository->all();

        return view('tags.index')
            ->with('tags', $tags);
    }

    /**
     * Show the form for creating a new tags.
     *
     * @return Response
     */
    public function create()
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 
        return view('tags.create');
    }

    /**
     * Store a newly created tags in storage.
     *
     * @param CreatetagsRequest $request
     *
     * @return Response
     */
    public function store(CreatetagsRequest $request)
    {
        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'nome' => 'required|string|max:255|unique:tags',
        ]);

        $input = $request->all();

        $tags = $this->tagsRepository->create($input);

        Flash::success('Tag salva com sucesso.');

        return redirect(route('tags.index'));
    }

    /**
     * Display the specified tags.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        $tags = $this->tagsRepository->findWithoutFail($id);

        if (empty($tags)) {
            Flash::error('Tag não encontrada.');

            return redirect(route('tags.index'));
        }

        return view('tags.show')->with('tags', $tags);
    }

    /**
     * Show the form for editing the specified tags.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        $tags = $this->tagsRepository->findWithoutFail($id);

        if (empty($tags)) {
            Flash::error('Tag não encontrada');

            return redirect(route('tags.index'));
        }

        return view('tags.edit')->with('tags', $tags);
    }

    /**
     * Update the specified tags in storage.
     *
     * @param  int              $id
     * @param UpdatetagsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatetagsRequest $request)
    {

        //VALIDAÇÃO DOS DADOS RECEBIDOS
        $request->validate([
            'nome' => 'required|string|max:255',
        ]);

        $tags = $this->tagsRepository->findWithoutFail($id);

        if (empty($tags)) {
            Flash::error('Tag não encontrada');

            return redirect(route('tags.index'));
        }

        if($tags->nome != $request->nome){
            $request->validate([
                'nome' => 'unique:tags',
            ]);
        }

        $tags = $this->tagsRepository->update($request->all(), $id);

        Flash::success('Tag atualizada com sucesso.');

        return redirect(route('tags.index'));
    }

    /**
     * Remove the specified tags from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        //checando se o cara eh admin pra ver pra onde ele vai
        if(Auth::user()->level_acl ==  1){
            return redirect('/home');
        } 

        $tags = $this->tagsRepository->findWithoutFail($id);

        if (empty($tags)) {
            Flash::error('Tag não encontrada');

            return redirect(route('tags.index'));
        }

        $this->tagsRepository->delete($id);

        Flash::success('Tags deletada com sucesso.');

        return redirect(route('tags.index'));
    }
}
