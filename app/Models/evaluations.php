<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class evaluations
 * @package App\Models
 * @version July 31, 2018, 9:40 pm UTC
 *
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection eventos
 * @property \Illuminate\Database\Eloquent\Collection ofertasMembros
 * @property string ip
 * @property integer nota
 * @property string nome
 * @property string conteudo
 * @property integer user_id
 */
class evaluations extends Model
{
    use SoftDeletes;

    public $table = 'evaluations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'ip',
        'nota',
        'favorito',
        'conteudo',
        'user_id',
        'evento_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ip' => 'string',
        'nota' => 'integer',
        'favorito' => 'boolean',
        'conteudo' => 'string',
        'user_id' => 'integer',
        'evento_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
    public function evento()
    {
        return $this->belongsTo(\App\Models\Evento::class);
    }
}
