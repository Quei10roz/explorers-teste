<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class eventos
 * @package App\Models
 * @version July 30, 2018, 11:36 pm UTC
 *
 * @property \App\Models\User user
 * @property string titulo
 * @property string estado
 * @property string cidade
 * @property date data
 * @property string link
 * @property string descricao
 * @property integer user_id
 */
class eventos extends Model
{
    use SoftDeletes;

    public $table = 'eventos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'slug',
        'privado',
        'numero_participantes',
        'tipo_eventos',
        'cidade',
        'data',
        'link',
        'descricao',
        'user_id',
        'oferta_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titulo' => 'string',
        'slug' => 'string',
        'privado' => 'boolean',
        'numero_participantes' => 'integer',
        'tipo_eventos' => 'string',
        'cidade' => 'string',
        'data' => 'date',
        'link' => 'string',
        'descricao' => 'string',
        'user_id' => 'integer',
        'oferta_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
