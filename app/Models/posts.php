<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class posts
 * @package App\Models
 * @version August 2, 2018, 4:57 pm UTC
 *
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection Comentario
 * @property \Illuminate\Database\Eloquent\Collection eventos
 * @property \Illuminate\Database\Eloquent\Collection ofertasMembros
 * @property \Illuminate\Database\Eloquent\Collection TagsPost
 * @property string titulo
 * @property string conteudo
 * @property string image
 * @property string link-video
 * @property integer user_id
 */
class posts extends Model
{
    use SoftDeletes;

    public $table = 'posts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'slug',
        'conteudo',
        'image',
        'link_video',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'titulo' => 'string',
        'slug' => 'string',
        'conteudo' => 'string',
        'image' => 'string',
        'link_video' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function comentarios()
    {
        return $this->hasMany(\App\Models\Comentario::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function tagsPosts()
    {
        return $this->hasMany(\App\Models\TagsPost::class);
    }
}
