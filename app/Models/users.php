<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class users
 * @package App\Models
 * @version July 30, 2018, 8:31 pm UTC
 *
 * @property string name
 * @property string email
 * @property string password
 * @property string code
 * @property string reputacao
 * @property string facebook
 * @property string instagram
 * @property string twitter
 * @property string linkedin
 * @property string estado
 * @property string cidade
 * @property string descricao
 * @property string slug
 * @property string remember_token
 */
class users extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'password',
        'code',
        'reputacao',
        'facebook',
        'instagram',
        'twitter',
        'linkedin',
        'image',
        'estado',
        'cidade',
        'descricao',
        'slug',
        'level_acl',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'code' => 'string',
        'reputacao' => 'integer',
        'facebook' => 'string',
        'instagram' => 'string',
        'twitter' => 'string',
        'linkedin' => 'string',
        'image' => 'string',
        'estado' => 'string',
        'cidade' => 'string',
        'descricao' => 'string',
        'slug' => 'string',
        'level_acl' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
