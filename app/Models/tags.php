<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class tags
 * @package App\Models
 * @version July 31, 2018, 9:02 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection eventos
 * @property \Illuminate\Database\Eloquent\Collection ofertasMembros
 * @property string nome
 */
class tags extends Model
{
    use SoftDeletes;

    public $table = 'tags';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
