<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ofertas
 * @package App\Models
 * @version July 30, 2018, 10:09 pm UTC
 *
 * @property string nome
 * @property string image
 * @property string descricao
 */
class ofertas extends Model
{
    use SoftDeletes;

    public $table = 'ofertas';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome',
        'slug',
        'image_perfil',
        'image_banner',
        'aprovado',
        'descricao',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'slug' => 'string',
        'image_perfil' => 'string',
        'image_banner' => 'string',
        'aprovado'=> 'string',
        'descricao' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
