<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class comentarios
 * @package App\Models
 * @version August 2, 2018, 4:58 pm UTC
 *
 * @property \App\Models\Post post
 * @property \Illuminate\Database\Eloquent\Collection eventos
 * @property \Illuminate\Database\Eloquent\Collection ofertasMembros
 * @property \Illuminate\Database\Eloquent\Collection tagsPosts
 * @property string autor
 * @property string conteudo
 * @property integer post_id
 */
class comentarios extends Model
{
    use SoftDeletes;

    public $table = 'comentarios';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'autor',
        'conteudo',
        'post_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'autor' => 'string',
        'conteudo' => 'string',
        'post_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function post()
    {
        return $this->belongsTo(\App\Models\Post::class);
    }
}
