<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ROTAS AREA GUEST
Route::get('/', 'PagesController@getIndex')->name('home');
Route::get('exploradores', 'PagesController@getMembros')->name('explorers');
Route::get('idealizador', 'PagesController@getIdealizador')->name('idealizador');
Route::get('exploradores/{slug}', 'PagesController@getPerfil')->name('explorer');

Route::get('quem-somos', 'PagesController@getQuemSomos')->name('quem-somos');
Route::get('origem', 'PagesController@getOrigem')->name('origem');
Route::get('bussulas', 'PagesController@getProduto')->name('bussulas');
Route::get('avalie', 'PagesController@getAvalie')->name('evento.avalaiacao');
Route::get('livro', 'PagesController@getLivro')->name('livro');
Route::get('diario', 'PagesController@getDiario')->name('diario');
Route::get('eventos/{slug}', 'PagesController@getEventos')->name('pagina.evento');
Route::get('bussulas/{slug}', 'PagesController@getOfertas')->name('bussula');


//ROTAS DO SISTEMA

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('auth');

//ROTAS DE USUARIO

Route::any('typeahead_user','usersController@typeahead_user');

Route::get('busca_users', 'usersController@indexBusca')->name('buscaUser')->middleware('auth');

Route::resource('users', 'usersController')->middleware('auth');

//ROTAS DE OFERTAS

Route::get('AprovarOfertasSugeridas', 'ofertasController@getAprovarOferta')->name('ofertas.aprovar')->middleware('auth');

Route::get('NegarOfertasSugeridas', 'ofertasController@getNegarOferta')->name('ofertas.negar')->middleware('auth');

Route::get('OfertasSugeridas', 'ofertasController@getOfertaSugestoes')->name('ofertas.sugestoes')->middleware('auth');

Route::get('SuasOfertas', 'ofertasController@getAreaMembro')->name('ofertas.cadastro')->middleware('auth');

Route::get('{id}/membros/busca_ofertas', 'ofertasController@getMembrosBusca')->name('buscaUser_Oferta')->middleware('auth');

Route::get('{id}/membros', 'ofertasController@getMembros')->name('ofertas.membros')->middleware('auth');

Route::get('{id}/add_membros/busca_ofertas', 'ofertasController@getAdd_membrosBusca')->name('buscaUser_Oferta_add')->middleware('auth');

Route::get('{id}/add_membros', 'ofertasController@getAdd_membros')->name('ofertas.add_membros')->middleware('auth');

Route::post('{id}/membros', 'ofertasController@postAdd_membros')->name('ofertas.store_membros')->middleware('auth');

Route::resource('ofertas', 'ofertasController')->middleware('auth');

//ROTAS DE EVENTOS

Route::get('avaliacao/evento/{id}', 'evaluationsController@indexAvaliacoesEvento')->name('eventos.avaliacoes')->middleware('auth');

Route::get('SeusEventos', 'eventosController@indexSeusEventos')->name('seuseventos')->middleware('auth');

Route::resource('eventos', 'eventosController')->middleware('auth');
 
//ROTAS DE TAG

Route::resource('tags', 'tagsController')->middleware('auth');

//ROTAS DE AVALIACAO

Route::get('SuasAvaliacoes', 'evaluationsController@indexSuasAvaliacoes')->name('suasavaliacoes')->middleware('auth');

Route::resource('evaluations', 'evaluationsController')->middleware('auth');

Route::get('{id}/avaliacao', 'evaluationsController@getFavorito')->name('evaluations.favorito')->middleware('auth');

//ROTAS DE POSTS

Route::any('typeahead_post','postsController@typeahead_post');

Route::get('busca_posts', 'postsController@indexBusca')->name('buscaPost')->middleware('auth');

Route::get('SeusPosts', 'postsController@indexSeusPosts')->name('seusposts')->middleware('auth');

Route::resource('posts', 'postsController')->middleware('auth');

//ROTAS DE COMENTARIOS

Route::get('{id}/comentarios', 'comentariosController@comentariosPost')->name('comentariospost')->middleware('auth');

Route::get('SeusComentarios', 'comentariosController@indexSeusComentarios')->name('seuscomentarios')->middleware('auth');

Route::resource('comentarios', 'comentariosController')->middleware('auth');